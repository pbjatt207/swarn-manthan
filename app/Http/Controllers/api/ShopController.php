<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Shop;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->firstOrFail();

        $re = [
            'status'    => true,
            'data'      => $shop
        ];

        return response()->json($re);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address'   => 'required',
            'city_id'   => 'required',
            'pincode'   => 'required',
            'name'      => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->all();
            $user = auth()->user();
            $shop = Shop::where('user_id', $user->id)->firstOrFail();
            if ($shop->fill($input)->save()) {
                $re = [
                    'message' =>  'Updated Successfully',
                    'data' => $shop
                ];
                return response()->json($re);
            } else {
                $re = [
                    'message' => 'Please try again'
                ];
                return response()->json($re, 403);
            }
        }

        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function latitude_longitude(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude'             => 'required',
            'longitude'         => 'required'
        ]);
        if ($validator->fails()) {
            $data = array();
            $data['status'] = 'failed';
            $data['data'] = $validator->errors();
            $data['msg'] = 'Invalid Perameters';
            return response()->json($data, 200);
        }
        $input = $request->all();
        $user = Auth::user();

        $shop = Shop::where('user_id', $user->id)->update(['latitude' => $input['latitude'], 'longitude' => $input['longitude']]);
        $data = Shop::where('user_id', $user->id)->first();

        $result = array();
        $result['status'] = 'success';
        $result['data'] = $data;
        $result['msg'] = 'successful added';
        return response()->json($result, 200);
    }
    public function marchant_list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city_id'             => 'required',
        ]);
        if ($validator->fails()) {
            $data = array();
            $data['status'] = 'failed';
            $data['data'] = $validator->errors();
            $data['msg'] = 'Invalid Perameters';
            return response()->json($data, 200);
        }
        $marchant = Shop::where('city_id', '=', $request->city_id)->whereHas('user', function ($q) {
            $q->where('status', '=', 'enable');
        })->get();

        $result = array();
        $result['status'] = 'success';
        $result['msg'] = 'Total records ' . count($marchant);
        $result['data'] = $marchant;
        return response()->json($result, 200);
    }
}
