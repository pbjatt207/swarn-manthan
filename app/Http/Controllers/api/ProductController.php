<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Image;

use App\Model\Product;
use App\Model\ProductImage;
use App\Model\Company;
use App\Model\Shop;
use App\Model\Setting;
use App\Model\PurchasePlan;
use App\Model\Notification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
	public function index(Request $request)
	{
		
		$validator = Validator::make($request->all(), []);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {
			$user = Auth::user();
			$shop = Shop::where('user_id', $user->id)->firstOrFail();
			$purchase_plan = PurchasePlan::where('status','Active')->where('shop_id',$shop->id)->first();
			
				// 			$lists = Product::latest()->with('shop')->where('shop_id', $shop->id)->get();
				$lists = DB::table('products')
					->select('products.*', 'categories.name as category_name', 'shops.name as shop_name')
					->leftjoin('categories', 'products.category_id', 'categories.id')
					->leftjoin('shops', 'products.shop_id', 'shops.id')
					->where('products.shop_id', '=', $shop->id)
					// ->where('shops.city_id', '=', $request->city_id)
					// ->addSelect(DB::raw("'d' as fakeColumn"))
					->latest()
					->get();
					
				
				// dd($show_front);
				foreach ($lists as $d) {
					// $couponimage = Couponimage::where('coupon_id',$d->id)->get();
					$productimage = DB::table('product_images')
						->where('product_images.product_id', '=', $d->id)
						->select('product_images.name as productimage', 'product_images.id')
						->get();

					$d->productimage = $productimage;
				}
				
				if ($lists->isEmpty()) {
					$re = [
						'status' => false,
						'message'	=> 'No record(s) found.'
					];
				} else {
					$re = [
						'status' 	=> true,
						'message'	=> $lists->count() . " records found.",
						'data'   	=> $lists
					];
				}
		}
		return response()->json($re);
	}
	public function product_detail(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'product_id'         		=> 'required',
		]);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {

			$product = Product::where('id', $request['product_id'])->count();
			if ($product > 0) {
				$product_detail = DB::table('products')
					->select('products.*', 'products.name as product_name', 'categories.name AS cat_name', 'shops.name AS shop_name')
					->leftjoin('categories', 'products.category_id', 'categories.id')
					->leftjoin('shops', 'products.shop_id', 'shops.id')
				// 	->where('shops.city_id', $request->city_id)
					->where('products.id', $request->product_id)
					->first();
				$re = [
					'status' 	=> true,
					'data'   	=> $product_detail
				];
			} else {
				$re = [
					'status' 	=> true,
					'message'	=> 'Product Not Found.',
				];
			}
		}





		return response()->json($re);
	}
	public function user_product_detail(Request $request)
	{
	    $setting = Setting::find(1);
		$user = auth()->user();
		$validator = Validator::make($request->all(), [
			'product_id'         		=> 'required',
		]);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {
			$pro = Product::find($request['product_id']);
			$pro->update(['views' => $pro->views + 1]);
			
			$product = Product::where('id', $request['product_id'])->count();
			if ($product > 0) {
				$product_detail = DB::table('products')
					->select('products.*', 'products.name as product_name', 'categories.name AS cat_name', 'shops.name AS shop_name')
					->leftjoin('categories', 'products.category_id', 'categories.id')
					->leftjoin('shops', 'products.shop_id', 'shops.id')
				// 	->where('shops.city_id', $request->city_id)
					->where('products.id', $request->product_id)
					->first();
				if ($product_detail) {

					$favorite = DB::table('favorite_product')
						->select('favorite_product.*')
						->where('product_id', $product_detail->id)
						->where('user_id', $user->id)
						->get();

					$product_detail->favorite = null;
					$product_detail->favorite_id = null;
					foreach ($favorite as $f) {

						$product_detail->favorite = $f ? 'yes' : 'no';
						$product_detail->favorite_id = $f ? $f->id : '';
					}

					// $shop_detail = DB::table('shops')
					//     ->select('shops.*','cities.name as city_name')
					//     ->leftjoin('cities','shops.city_id' ,'cities.id')
					//     ->where('shops.id','=',$product_detail->shop_id)
					//     ->first();
					$shop_detail = Shop::with('user', 'city')->where('id', $product_detail->shop_id)->first();
					//             $similar_cat_product = DB::table('products')
					// 		->select('products.*','products.name as product_name','categories.name AS cat_name','shops.name AS shop_name')
					// 		->leftjoin('categories','products.category_id' ,'categories.id')
					// 		->leftjoin('shops','products.shop_id' ,'shops.id')
					//     				->where('products.category_id', $product_detail->category_id)
					//                     ->latest()->take(5)->get();

					$similar_cat_product = Product::whereHas('shop', function ($r) use ($request) {
						$r->where('city_id', '=', $request->city_id);
					})->with('category')
						->where('category_id', $product_detail->category_id)
						->where('id', '!=', $product_detail->id)
						->latest()
						->take(5)
						->get();
					foreach ($similar_cat_product as $p) {
						//   $p->products_description = strip_tags($p->products_description);
						$user_id                = auth()->user()->id;
						$favorite = DB::table('favorite_product')
							->select('favorite_product.*')
							->where('product_id', $p->id)
							->where('user_id', $user->id)
							->get();

						$p->favorite = null;
						$p->favorite_id = null;
						foreach ($favorite as $f) {

							$p->favorite = $f ? 'yes' : 'no';
							$p->favorite_id = $f ? $f->id : '';
						}
					}
				// 	dd($request->city_id);
				
				    $purchase_plan = PurchasePlan::where('shop_id',$product_detail->shop_id)->with('plan')->where('status','Active')->first();
				    if( $purchase_plan){
				        
					    $product_limit = $purchase_plan->plan->product_limit - 1;
    					$similar_shop_product = Product::whereHas('shop', function ($r) use ($request) {
    						$r->where('city_id', '=', $request->city_id);   
    					})->with('category')
    						->where('shop_id', $product_detail->shop_id)
    						->latest()
    						->take($product_limit)
    						->get();
				    } else {
				        $product_limit = $setting->product_show_limit - 1;
    					$similar_shop_product = Product::whereHas('shop', function ($r) use ($request) {
    						$r->where('city_id', '=', $request->city_id);   
    					})->with('category')
    						->where('shop_id', $product_detail->shop_id)
    						->latest()
    						->take($product_limit)
    						->get();
				    }
					foreach ($similar_shop_product as $p) {
						//   $p->products_description = strip_tags($p->products_description);
						$user_id                = auth()->user()->id;
						$favorite = DB::table('favorite_product')
							->select('favorite_product.*')
							->where('product_id', $p->id)
							->where('user_id', $user->id)
							->get();

						$p->favorite = null;
						$p->favorite_id = null;
						foreach ($favorite as $f) {

							$p->favorite = $f ? 'yes' : 'no';
							$p->favorite_id = $f ? $f->id : '';
						}
					}
					//             $similar_shop_product = DB::table('products')
					// 		->select('products.*','products.name as product_name','categories.name AS cat_name','shops.name AS shop_name')
					// 		->leftjoin('categories','products.category_id' ,'categories.id')
					// 		->leftjoin('shops','products.shop_id' ,'shops.id')
					//     				->where('products.shop_id', $product_detail->shop_id)
					//                     ->latest()->take(5)->get();
					$re = [
						'status' 	=> true,
						'data'   	=> $product_detail,
						'shop_detail' => $shop_detail,
						'similar_cat_product' => $similar_cat_product,
						'similar_shop_product' =>  $similar_shop_product
					];
				} else {
					$re = [
						'status' 	=> true,
						'message'	=> 'Product Not Found.',
					];
				}
			}
		}





		return response()->json($re);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name'         		=> 'required',
			'category_id'       => 'required',
			'price'        		=> 'required',
			'description'       => 'required',
			'image' 			=> 'nullable|image|mimes:jpeg,png,jpg',
		]);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {

			$user = Auth::user();
			$shop = Shop::where('user_id', $user->id)->firstOrFail();
			$purchase_plan = PurchasePlan::where('shop_id',$shop->id)->where('status','Active')->first();
			$product_count = Product::where('shop_id',$shop->id)->get();
			
			if($purchase_plan != null){
				if(count($product_count) < $purchase_plan->plan->product_limit){
					// dd('an');
					$product 					= new Product;
					$product->shop_id    		= $shop->id;
					$product->category_id    	= $request->category_id;
					$product->name    			= $request->name;
					$product->price   			= $request->price;
					$product->description   	= $request->description;
					$product->material          = $request->material;
					$product->product_weight    = $request->product_weight;
					$product->diamond_weight    = $request->diamond_weight;
					$product->size              = $request->size;
					$product->slug    = $request->slug == '' ? Str::slug($request->name . '-') : $request->slug;
		
					if ($file = $request->file('image')) {
						$optimizeImage = Image::make($file);
						$optimizePath = public_path() . '/imgs/product/';
						$name = time() . $file->getClientOriginalName();
						$optimizeImage->save($optimizePath . $name, 72);
						$product->image    			= $name;
					}
		
		
					// if ($file = $request->file('image2')) {
					// 	$optimizeImage = Image::make($file);
					// 	$optimizePath = public_path() . '/images/products/';
					// 	$name2 = time() . $file->getClientOriginalName();
					// 	$optimizeImage->save($optimizePath . $name2, 72);
		
					// 	$product->image2    	= $name2;
					// }
					// if ($file = $request->file('image3')) {
					// 	$optimizeImage = Image::make($file);
					// 	$optimizePath = public_path() . '/images/products/';
					// 	$name3 = time() . $file->getClientOriginalName();
					// 	$optimizeImage->save($optimizePath . $name3, 72);
		
					// 	$product->image3    	= $name3;
					// }
					// if ($file = $request->file('image4')) {
					// 	$optimizeImage = Image::make($file);
					// 	$optimizePath = public_path() . '/images/products/';
					// 	$name4 = time() . $file->getClientOriginalName();
					// 	$optimizeImage->save($optimizePath . $name4, 72);
		
					// 	$product->image4    	= $name4;
					// }
					// if ($file = $request->file('image5')) {
					// 	$optimizeImage = Image::make($file);
					// 	$optimizePath = public_path() . '/images/products/';
					// 	$name5 = time() . $file->getClientOriginalName();
					// 	$optimizeImage->save($optimizePath . $name5, 72);
		
					// 	$product->image5    	= $name5;
					// }
		
		
					$product->save();

					$shop = Shop::find($product->shop_id);
					$notification = new Notification();
					$notification->product_id = $product->id;
					$notification->shop_id    = $product->shop_id;
					$notification->message    = 'New product '.$product->name.' has been added by '.$shop->name;
					$notification->type       = 'User';
					$notification->save();
		
					$updateProduct = Product::where('id', $product->id)->update(['slug' => $product->name . '-' . $product->id]);
					$re = [
						'status' 	=> true,
						'message'	=> 'Product added successfully.',
						'data'   	=> $product
					];
				} else {
					$re = [
						'status'	=> false,
						'message'	=> 'Product quantity limit exceeded shop'
					];
				}

			} else {
				$re = [
					'status'	=> false,
					'message'	=> 'Not a valid plan'
				];
			}
			// dd($shop);

		}
		return response()->json($re);
	}

	public function show(Request $request, Product $product)
	{
		$re = [
			'status' 	=> true,
			'data'   	=> $product
		];

		return response()->json($re);
	}

	public function update(Request $request, Product $product)
	{
		$validator = Validator::make($request->all(), [
			'name'         		=> 'required',
			'category_id'       => 'required',
			'price'        		=> 'required',
			'description'       => 'required',
			'image' 			=> 'nullable|image|mimes:jpeg,png,jpg',
		]);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {
			$user = Auth::user();
			$shop = Shop::where('user_id', $user->id)->firstOrFail();

			$product->shop_id    		= $shop->id;
			$product->category_id    	= $request->category_id;
			$product->name    			= $request->name;
			$product->price   			= $request->price;
			$product->description   	= $request->description;
			$product->material          = $request->material;
			$product->product_weight    = $request->product_weight;
			$product->diamond_weight    = $request->diamond_weight;
			$product->size              = $request->size;
			$product->slug    = $request->slug == '' ? Str::slug($request->name . '-' . $product->id, '-') : $request->slug;

			if ($request->file('image') != '') {
				if ($file = $request->file('image')) {
					$optimizeImage = Image::make($file);
					$optimizePath = public_path() . '/images/product/';
					$name = time() . $file->getClientOriginalName();
					$optimizeImage->save($optimizePath . $name, 72);

					$product->image    = $name;
				}
			}
			// if ($request->file('image2') != '') {
			// 	if ($file = $request->file('image2')) {
			// 		$optimizeImage = Image::make($file);
			// 		$optimizePath = public_path() . '/images/products/';
			// 		$name2 = time() . $file->getClientOriginalName();
			// 		$optimizeImage->save($optimizePath . $name2, 72);

			// 		$product->image2    = $name2;
			// 	}
			// }
			// if ($request->file('image3') != '') {
			// 	if ($file = $request->file('image3')) {
			// 		$optimizeImage = Image::make($file);
			// 		$optimizePath = public_path() . '/images/products/';
			// 		$name3 = time() . $file->getClientOriginalName();
			// 		$optimizeImage->save($optimizePath . $name3, 72);

			// 		$product->image3    = $name3;
			// 	}
			// }
			// if ($request->file('image4') != '') {
			// 	if ($file = $request->file('image4')) {
			// 		$optimizeImage = Image::make($file);
			// 		$optimizePath = public_path() . '/images/products/';
			// 		$name4 = time() . $file->getClientOriginalName();
			// 		$optimizeImage->save($optimizePath . $name4, 72);

			// 		$product->image4    = $name4;
			// 	}
			// }
			// if ($request->file('image5') != '') {
			// 	if ($file = $request->file('image5')) {
			// 		$optimizeImage = Image::make($file);
			// 		$optimizePath = public_path() . '/images/products/';
			// 		$name5 = time() . $file->getClientOriginalName();
			// 		$optimizeImage->save($optimizePath . $name5, 72);

			// 		$product->image5    = $name5;
			// 	}
			// }

			$product->save();
			$re = [
				'status' 	=> true,
				'message'	=> 'Product updated successfully.',
				'data'   	=> $product
			];
		}
		return response()->json($re);
	}

	public function destroy(Request $request, Product $product)
	{
		$product->delete();

		$re = [
			'status' 	=> true,
			'message'	=> 'Product deleted successfully.',
		];
		return response()->json($re);
	}
	public function remove_product_image(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'product_id' 			=> 'required',
			'image_id'              => 'required',
		]);

		if ($validator->fails()) {
			$data = array();
			$data['status'] = 'failed';
			$data['data'] = $validator->errors();
			$data['msg'] = 'Invalid Perameters';
			return response()->json($data, 200);
		}
		$input = $request->all();
		$count_image = ProductImage::where('id', $input['image_id'])->where('product_id', $input['product_id'])->count();
		if ($count_image == '1') {
			$product_image = ProductImage::where('id', $input['image_id'])->where('product_id', $input['product_id'])->delete();
			$re = [
				'status' 	=> true,
				'message'	=> 'Product deleted successfully.',
			];
			return response()->json($re);
		} else {
			$re = [
				'status' 	=> true,
				'message'	=> 'Record Not Found',
			];
			return response()->json($re);
		}
	}

	public function add_product_image(Request $request)
	{

		$validator = Validator::make($request->all(), [
			'product_id' 			=> 'required',
		]);

		if ($validator->fails()) {
			$data = array();
			$data['status'] = 'failed';
			$data['data'] = $validator->errors();
			$data['msg'] = 'Invalid Perameters';
			return response()->json($data, 200);
		}
		$input = $request->all();
		$ProductimageList = ProductImage::where('product_id', '=', $input['product_id'])->get();
		if (count($ProductimageList) <= 3) {

			// dd($request->file('name'));
			if ($file = $request->file('name')) {

				$optimizeImage = Image::make($file);
				$optimizePath = public_path() . '/imgs/product/';
				$name = time() . $file->getClientOriginalName();
				$optimizeImage->save($optimizePath . $name, 72);

				$input['name'] = $name;
			

			$Data = new ProductImage($input);




			//	$Data  = DB::table('order_trans')
			//					->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem',
			//						'coupons.image','coupons.expiry','stores.title as store_name')
			//					->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
			//					->leftjoin('stores','order_trans.store_id' ,'stores.id')
			//					->where('order_trans.store_id' ,'=' , $input['store_id'])
			//					->where('is_redeem' ,'=' ,'N')
			//					->get();




			if ($Data->save()) {
				$result = array();
				$result['status'] = 'success';
				$result['data'] = $Data;
				$result['msg'] = 'successful added';
				return response()->json($result, 200);
			} else {

				$data = array();
				$data['status'] = 'failed';
				$data['data'] = [];
				$data['msg'] = 'No records Found';
				return response()->json($data, 200);
				//return response()->json(['Failed'=>'Failed']); 
			}
		} else {

			$data = array();
				$data['status'] = 'failed';
				$data['data'] = [];
				$data['msg'] = 'Your image limit exist';
				return response()->json($data, 200);
		}

		} else {

			$data = array();
			$data['status'] = 'failed';
			$data['data'] = [];
			$data['msg'] = 'Autharization Failed';
			return response()->json($data, 200);
		}
	}

	public function Product_list(Request $request)
	{
		$user = Auth::user();
		$purchase_plan = PurchasePlan::where('status','Active')->get();
		// dd($purchase_plan);
		// $lists = Product::with('user','shop')->get();

		$listArr = [];

		foreach($purchase_plan as $pp){
			$lists = Product::whereHas('shop', function ($r) use ($request) {
				$r->where('city_id', '=', $request->city_id);
			})->where('shop_id',$pp->shop_id)->where('status',true)->with('category', 'user')->take($pp->plan->product_limit)->get()->toArray();
			
			// dd($lists);
			foreach ($lists as $key => $list) {
				$f = DB::table('favorite_product')
					->select('favorite_product.*')
					->where('product_id', $list['id'])
					->where('user_id', $user->id)
					->first();
				// dd($user->id);	
	
				$lists[$key]['favorite'] = $f && $f->id ? 'yes' : 'no';
				$lists[$key]['favorite_id'] = $f && $f->id ? $f->id : '';
			}

			$listArr = array_merge($listArr, $lists);
		}

		if (empty($listArr)) {
			$re = [
				'status' => false,
				'message'    => 'No record(s) found.'
			];
		} else {
			$re = [
				'status'     => true,
				'message'    => count($listArr) . " records found.",
				'data'       => $listArr
			];
		}

		return response()->json($re);
	}
	
	public function Search_product(Request $request)
	{
		// $user = Auth::user();
		// $query = Product::with('user','shop')->latest();

		$query = Product::whereHas('shop', function ($r) use ($request) {
			$r->where('city_id', '=', $request->city_id);
		})->with('category', 'user')->latest();

		if (!empty($request->name)) {
			$query->where('name', 'LIKE', '%' . $request->name . '%');
		}
		$lists = $query->get();
		// $lists = Product::with('user','shop')->where('name',$request->name)->get();


		if ($lists->isEmpty()) {
			$re = [
				'status' => false,
				'message'    => 'No record(s) found.'
			];
		} else {
			$re = [
				'status'     => true,
				'message'    => $lists->count() . " records found.",
				'data'       => $lists
			];
		}

		return response()->json($re);
	}
}
