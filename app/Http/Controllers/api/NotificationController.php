<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Enquery;
use Illuminate\Http\Request;
use Validator;

use App\Model\Notification;
use App\Model\Shop;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
	public function Marchant_notification(Request $request)
	{
		$user = Auth::user();
		// dd($user);
		$shop = Shop::where('user_id', $user->id)->firstOrFail();
		// dd($shop);

		$lists = Notification::latest()->where('shop_id', $shop->id)->where('type','Marchant')->get();
		// dd($lists);
		foreach ($lists as $key => $list) {
			if($list->enquery_id != null){
			$enquery = Enquery::with('enquery_product')->findOrFail($list->enquery_id);
			$list->enquery_detail = $enquery;
			}
		}

		if ($lists->isEmpty()) {
			$re = [
				'status' => false,
				'message'	=> 'No record(s) found.'
			];
		} else {
			$re = [
				'status' => true,
				'message'	=> $lists->count() . " records found.",
				'data'   => $lists
			];
		}

		return response()->json($re);
	}
	public function User_notification(Request $request)
	{
		$user = Auth::user();		

		$lists = Notification::latest()->where('type','User')->get();
		

		if ($lists->isEmpty()) {
			$re = [
				'status' => false,
				'message'	=> 'No record(s) found.'
			];
		} else {
			$re = [
				'status' => true,
				'message'	=> $lists->count() . " records found.",
				'data'   => $lists
			];
		}

		return response()->json($re);
	}

	public function show(Notification $notification)
	{
		$lists = $notification;
		// dd($lists);
		if($lists->enquery_id != null){
			$enquery = Enquery::with('enquery_product')->findOrFail($lists->enquery_id);
			$lists->enquery_detail = $enquery;
		}
		

		$re = [
			'status' 	=> true,
			'data'   	=> $lists
		];

		return response()->json($re);
	}
}
