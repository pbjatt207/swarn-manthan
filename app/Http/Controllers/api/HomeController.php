<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\City;
use App\Model\Enquery;
use Illuminate\Http\Request;
use Validator;

use App\Model\Notification;
use Illuminate\Support\Facades\DB;
use App\Model\Offer;
use App\Model\PurchasePlan;
use App\Model\Product;
use App\Model\Shop;
use App\Model\Setting;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
	public function index(Request $request)
	{
	   // dd($request->city_id);
	   $setting = Setting::find(1);
	   
	    $user = auth()->user();
		$offer = Offer::latest()->get();
		foreach($offer as $index => $arr){
                // dd($arr);
                foreach($arr as $key => $value){
                    // dd($value);
                    if($value == null){
                     $offer[$index]->{$key}= ''; 
                    }
                }
            }
// 		$category = Category::latest()->get();
		$category = DB::table('categories')->get();
// 		dd($category);
		foreach($category as $index => $arr){
                // dd($arr);
                foreach($arr as $key => $value){
                    // dd($value);
                    if($value == null){
                     $category[$index]->{$key}= ''; 
                    }
                }
            }

		// dd($purchase_plan);
		// $lists = Product::with('user','shop')->get();

		$listArr = [];
		$product = '';
		$shops = Shop::get();
        
    	foreach($shops as $shop){
		        $purchase_plan = PurchasePlan::where('status','Active')->with('plan')->where('shop_id',$shop->id)->first();
    			    
    			$list = Product::whereHas('shop', function ($r) use ($request) {
    				$r->where('city_id', '=', $request->city_id);
    			})->where('shop_id',$shop->id)->where('status',true)->with('category', 'user');
        			
    		    if($purchase_plan){
    		          //  dd($purchase_plan);
        			    $list = $list->take($purchase_plan->plan->product_limit);
    			} else {
    			    
    			        $list = $list->take($setting->product_show_limit);
    			}
    			
    			
        		$lists = $list->get()->toArray();
        			
        // 			dd($pp->plan->product_limit);
        			foreach ($lists as $key => $list) {
        				$f = DB::table('favorite_product')
        					->select('favorite_product.*')
        					->where('product_id', $list['id'])
        					->where('user_id', $user->id)
        					->first();
        				// dd($user->id);	
        	
        				$lists[$key]['favorite'] = $f && $f->id ? 'yes' : 'no';
        				$lists[$key]['favorite_id'] = $f && $f->id ? $f->id : '';
        			}
        
        			$listArr = array_merge($listArr, $lists);
        			$product = $listArr;
    		}
        

		
		


// 		$product = DB::table('products')
// 						->select('products.*','products.name as product_name','shops.*','categories.name AS cat_name','cities.name AS city_name','users.name AS user_name','users.mobile AS user_mobile','users.email AS user_email','shops.latitude','shops.longitude')
// 						->leftjoin('categories','products.category_id' ,'categories.id')
// 						->leftjoin('shops','products.shop_id' ,'shops.id')
// 						->leftjoin('cities','shops.city_id' ,'cities.id')
// 						->leftjoin('users','shops.user_id' ,'users.id')
//                         ->get();

		$homeArr = [];

		$homeArr[0] = [
        	'title'	=> "offers",
        	'data'	=> $offer
        ];
		$homeArr[1] = [
        	'title'	=> "category",
        	'data'	=> $category
        ];
		$homeArr[2] = [
        	'title'	=> "product",
        	'data'	=> $product
        ];
        $homeArr[3] = [
            'title' => 'setting',
            'data'  => $setting
        ];

		$re = [
			'status' => true,
			'data'   => $homeArr
		];
		return response()->json($re);
	}

	public function city()
	{
		$lists = City::latest()->with('state')->get();
		
		foreach($lists as $index => $arr){
                foreach($arr as $key => $value){
                    if($value == null){
                        
                     $lists[$index]->{$key}= ''; 
                    }
                }
            }

		$re = [
			'status' 	=> true,
			'data'   	=> $lists
		];

		return response()->json($re);
	}
}
