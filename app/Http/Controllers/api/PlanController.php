<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Image;

use App\Model\Plan;
use App\Model\PurchasePlan;
use App\Model\Shop;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class PlanController extends Controller
{
    public function plan_list(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'type'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $lists = Plan::where('type',$request->type)->latest()->get();

            $re = [
                'status'    => true,
                'message'   => 'total plans '.count($lists),
                'data'      => $lists
            ];
        }
        return response()->json($re);
    }
    public function PurchasePlan(Request $request)
    {
        
        
        $validator = Validator::make($request->all(), [
            'payment'   => 'required',
            'plan_id'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user = auth()->user();
            $shop = Shop::where('user_id', $user->id)->first();
            // dd($shop->id);
            if($shop != null){

                $plan_detail = Plan::find($request->plan_id);
                
                
                if($plan_detail->time_limit_in == 'Day'){
                    $expiry_date = Carbon::now()->addDays($plan_detail->time_limit);
                }
                if($plan_detail->time_limit_in == 'Month'){
                    $expiry_date = Carbon::now()->addMonths($plan_detail->time_limit);
                }
                if($plan_detail->time_limit_in == 'Year'){
                    $expiry_date = Carbon::now()->addYears($plan_detail->time_limit);
                }
                
                $input                      = $request->all();
                $input['shop_id']           = $shop->id;
                $input['subscription_date'] = Carbon::now();
                $input['expiry_date']       = $expiry_date;
                $count_purchase_plan        = PurchasePlan::where('shop_id', $shop->id)->get();
                // dd(count($count_purchase_plan));
                if(count($count_purchase_plan) == 0){
                    $obj = new PurchasePlan();
                    $obj->fill($input);
                    // $obj->save();

                    if($obj->save()){
                        $re = [
                            'status'    => true,
                            'message'   => 'New record add successful',
                        ];
                    } else {
                        $re = [
                            'status'    => false,
                            'message'   => 'Error',
                        ];
                    }
                } else {
                    $obj = PurchasePlan::where('shop_id',$shop->id)->first();
                    $obj->fill($input);
                    // $obj->save();

                    if($obj->save()){
                        $re = [
                            'status'    => true,
                            'message'   => 'Record update successful',
                        ];
                    } else {
                        $re = [
                            'status'    => false,
                            'message'   => 'Error',
                        ];
                    }
                }

            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Not shop Registered'
                ];
            }
            
            
            
        }
        return response()->json($re);
    }
}