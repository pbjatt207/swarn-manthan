<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Enquery;
use App\Model\EnqueryProduct;
use App\Model\Notification;
use App\Model\Product;
use App\Model\Shop;
use App\Model\Setting;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class EnqueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->first();
        $lists = Enquery::where('shop_id', $shop->id)->get();
        foreach ($lists as $key => $list) {
            $enquery_product = EnqueryProduct::where('enquery_id',$list->id)->firstOrFail();
            $list->product_detail = $enquery_product;
        }
        
        
        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status'     => true,
                'message'    => $lists->count() . " records found.",
                'data'       => $lists
            ];
        }

        return response()->json($re);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $validator = Validator::make($request->all(), [
		// 	'product_id'    => 'required',
		// 	'user_id'       => 'required',
		// ]);
		// if ($validator->fails()) {
		// 	$re = [
		// 		'status'    => false,
		// 		'message'   => 'Validations errors found.',
		// 		'errors'    => $validator->errors()
		// 	];
		// } else {

		// 	$user = Auth::user();
		// 	$product = Product::findOrFail($request->product_id);

		// 	$enquery 					= new Enquery();
		// 	$enquery->product_id        = $product->id;
		// 	$enquery->shop_id    	    = $product->shop_id;
		// 	$enquery->user_id    	    = $user->id;
		// 	$enquery->description    	= $user->name .' is enquiry for '. $product->name;
		// 	$enquery->name    	        = $user->name;
		// 	$enquery->mobile    	    = $user->mobile;
		// 	$enquery->email         	= $user->email;
		// 	$enquery->save();
            
		// 	$enquery_product 					= new EnqueryProduct();
		// 	$enquery_product->enquery_id    	= $enquery->id;
		// 	$enquery_product->category_id    	= $product->category_id;
		// 	$enquery_product->name    			= $product->name;
		// 	$enquery_product->price   			= $product->price;
		// 	$enquery_product->description   	= $product->description;
		// 	$enquery_product->image   	        = $product->image;
		// 	$enquery_product->save();

		// 	$notification 					= new Notification();
		// 	$notification->enquery_id    	= $enquery->id;
		// 	$notification->shop_id    	    = $product->shop_id;
        //     $notification->user_id          = $user->id;
		// 	$notification->message    	    = $user->name .' is enquiry for '. $product->name;
		// 	$notification->save();

        //     $enquery->product_detail = $enquery_product;

		// 	$re = [
		// 		'status' 	=> true,
		// 		'message'	=> 'Enquery added successfully.',
		// 		'data'   	=> $enquery
		// 	];
		// }
		// return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'product_id'    => 'required',
			// 'user_id'       => 'required',
		]);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {

			$user = Auth::user();
			$product = Product::findOrFail($request->product_id);

			$enquery 					= new Enquery();
			$enquery->product_id        = $product->id;
			$enquery->shop_id    	    = $product->shop_id;
			$enquery->user_id    	    = $user->id;
			$enquery->description    	= $user->name .' is enquiry for '. $product->name;
			$enquery->name    	        = $user->name;
			$enquery->mobile    	    = $user->mobile;
			$enquery->email         	= $user->email;
			$enquery->save();
            
			$enquery_product 					= new EnqueryProduct();
			$enquery_product->enquery_id    	= $enquery->id;
			$enquery_product->category_id    	= $product->category_id;
			$enquery_product->name    			= $product->name;
			$enquery_product->price   			= $product->price;
			$enquery_product->description   	= $product->description;
			$enquery_product->image   	        = $product->image;
			$enquery_product->save();

			$notification 					= new Notification();
			$notification->enquery_id    	= $enquery->id;
			$notification->shop_id    	    = $product->shop_id;
			$notification->type    	    = 'Marchant';
            // $notification->user_id          = $user->id;
			$notification->message    	    = $user->name .' is enquiry for '. $product->name;
			$notification->save();

            $enquery->product_detail = $enquery_product;
            
            $setting = Setting::find(1);
            $w_email = $setting->email;
            $sendmail = $user->email;
            $w_name = $setting->title;
            $subject = "Your Enquiry Send Successful";
            
            
            $data = array('w_email' => $w_email, 'email' => $sendmail, 'name' => $user->name, 'subject' => $subject, 'w_name' => $w_name);
           
                  Mail::send('email.mail', $data, function($message) use ($data) {
                  
                     $message->to($data['email'], $data['name'])->subject($data['subject']);
                     $message->from($data['w_email'], $data['w_name']);
                  });

			$re = [
				'status' 	=> true,
				'message'	=> 'Enquery added successfully.',
				'data'   	=> $enquery
			];
		}
		return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userenquery()
    {
        $user = Auth::user();
        $lists = Enquery::with('user')->where('user_id', $user->id)->get();
        foreach ($lists as $key => $list) {
            $enquery_product = EnqueryProduct::where('enquery_id',$list->id)->firstOrFail();
            $list->product_detail = $enquery_product;
        }
        
        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status'     => true,
                'message'    => $lists->count() . " records found.",
                'data'       => $lists
            ];
        }

        return response()->json($re);
    }
}
