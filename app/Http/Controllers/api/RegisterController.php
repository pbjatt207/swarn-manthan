<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Model\Setting;
use App\Model\Point;
use App\Model\Role;
use Illuminate\Support\Facades\DB;
use App\User;

use Hash;

class RegisterController extends Controller
{
    protected function referalcode($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'             => 'required|string|regex:/[A-Za-z ]+/',
            'mobile'            => 'required|string|regex:/\d{10}/',
            'email'             => 'required|email',
            'is_termscondition' => 'required'
        ]);


        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {

            $UserData =  User::where('mobile', $request->mobile)->whereHas('role', function ($q) {
                $q->where('name', 'User');
            })->first();

            $check  =  User::where('mobile', $request->mobile)->whereHas('role', function ($q) {
                $q->where('name', 'User');
            })->count();
            $role = Role::where('name', 'User')->first();
            if ($request->accept_code) {
                $referalcode_check =  User::where('referal_code', $request->accept_code)->get()->count();
            } else {
                $referalcode_check  =   1;
            }

            // $fname = strtoupper(substr($request->fname, 0, 3));

            if ($check == 0) {
                $dataArr = [
                    'name'    => request('name'),
                    'email'    => request('email'),
                    'mobile'   => request('mobile'),
                    'password' => Hash::make('123456'),
                    'device_id' => request('device_id'),
                    'device_type' => request('device_type'),
                    'fcm_id'    => request('fcm_id'),
                    // 'google_id'    => request('google_id'),
                    // 'fb_id'    => request('fb_id'),
                    // 'social_type'    => request('social_type'),
                    'is_termscondition' => request('is_termscondition'),
                    'role_id'     => $role->id,
                    'city_id'     => request('city_id'),
                    'accept_code'  => $request->accept_code,
                    'referal_code' => trim(request('name') . '' . $this->referalcode())
                ];
                if ($referalcode_check != 0) {
                    $newUser    = User::create($dataArr);
                    $setting = Setting::find(1);
                    $otp        = rand(100000, 999999);


                    // Send SMS
                    $msg    = urlencode("Dear " . $setting->title . " user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards " . $setting->title);
                    // $msg    = urlencode("Your one time password in " . $setting->title . " is " . $otp . " ");
                    $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$msg, request('mobile')], $setting->sms_api);
                    $sms    = file_get_contents($apiUrl);
                    $newUser->otp = $otp;
                    $newUser->save();

                    $setting->accept_point = $setting->accept_point == '' ? 0 : $setting->accept_point;
                    $setting->invite_point = $setting->invite_point == '' ? 0 : $setting->invite_point;

                    if ($newUser->accept_code != '') {
                        $point          =   new Point();
                        $point->point   =   $setting->accept_point;
                        $point->user_id     =   $newUser->id;
                        $point->save();

                        $user_check = User::where('referal_code', $newUser->accept_code)->first();
                        $user_point = Point::where('user_id', $user_check->id)->first();
                        if (!empty($user_point)) {
                            $user_point->point = $user_point->point + $setting->invite_point;
                            $user_point->save();
                        } else {
                            $point          =   new Point();
                            $point->point   =   $setting->invite_point;
                            $point->user_id     =   $newUser->id;
                            $point->save();
                        }
                    }

                    $obj =  User::where('mobile', request('mobile'))->first();
                    // dd($obj);
                    $obj->is_verified  = true;
                    $obj->password = Hash::make($otp);
                    $obj->save();

                    Auth::attempt(['mobile' => request('mobile'), 'password' => $newUser->otp]);
                    $user   = Auth::user();

                    $user_detail = DB::table('users')
                        ->select('users.*', 'cities.name as city_name')
                        ->leftjoin('cities', 'users.city_id', 'cities.id')
                        ->where('users.id', '=', $newUser->id)
                        ->first();

                    $token  = $user->createToken('SwarnManthan')->accessToken;

                    $re = [
                        'status'    => true,
                        'message'   => 'Success!! Registration successful.',
                        'otp' => $otp,
                        'token' => $token,
                        'user_details' => $user_detail,
                    ];
                } else {
                    $re = [
                        'status'    => false,
                        'message'   => 'Referal Code not valid.',
                    ];
                }
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Mobile number already exists.',
                    'user_details' => $UserData
                ];
            }
        }
        return response()->json($re);
    }

    public function sendOtp(Request $request)
    {
        $setting = Setting::find(1);
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|numeric|regex:/\d{10}/',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $otp = rand(100000, 999999);
            $message    = urlencode("Dear " . $setting->title . " user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards " . $setting->title);

            // $message = urlencode("Your One Time Password (OTP) is {$otp} for {$setting->title}.");
            // print_r($setting);
            // $sms_url = str_replace(["[MOBILE]", "[MESSAGE]"], [request('mobile'), $message], $setting->sms_api);
            // die;

            // $sms = file_get_contents($sms_url);

            $user = User::where('mobile', request('mobile'))->where('role_id',2);

            if ($user->count()) {
                $user->first();
                $dataArr = [
                    'password'  => bcrypt($otp),
                    'otp' => $otp,
                ];
                $user->update($dataArr);
                
            }

            $re = [
                'status'    => true,
                'message'   => 'OTP has been sent to ' . request('mobile'),
                'otp_code'  => $otp,
                'isExists'  => $user->count()
            ];
            $code = 200;
        }
        return response()->json($re, $code);
    }
    public function verifyOtp(Request $request)
    {
        $OTP = request('otp_code');
        $mobile = request('mobile');

        if ($OTP == request('otp')) {

            $UserData =  User::where('mobile', $mobile)->first();

            $obj =  User::where('mobile', $mobile)->first();
            $obj->is_verified  = true;
            $obj->save();

            $re = [
                'status'    => true,
                'is_verified'    => true,
                'message'       => 'Success!!  Account verified. Please login.',
                'user_details' => $UserData
            ];
        } else {
            $re = [
                'status'    => false,
                'message'       => 'Error!!  OTP not match.'
            ];
        }
        return response()->json($re);
    }

    public function forgot_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new_password'     => 'required|string|min:8|same:new_password',
            'confirm_password' => 'required|string|min:8|same:new_password'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {

            $info = User::where('mobile', request('mobile'))->first();
            $password  =  $info->password;

            $ID  =  $info->id;
            $lists         = User::find($ID);

            $new_password = Hash::make($request->new_password);

            $details = $lists->fill(['password' => $new_password])->save();

            $re = [
                'status'    => true,
                'message'   => 'Success! Password has been updated.',
                'data'      =>  $lists
            ];
        }
        return response()->json($re);
    }
}
