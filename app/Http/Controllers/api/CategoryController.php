<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Enquery;
use Illuminate\Http\Request;
use Validator;

use App\Model\Notification;
use App\Model\PurchasePlan;
use App\Model\Product;
use App\Model\Shop;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
	public function index(Request $request)
	{
		$lists = Category::latest()->get();

		if ($lists->isEmpty()) {
			$re = [
				'status' => false,
				'message'	=> 'No record(s) found.'
			];
		} else {
			$re = [
				'status' => true,
				'message'	=> $lists->count() . " records found.",
				'data'   => $lists
			];
		}

		return response()->json($re);
	}

	public function product(Category $id, Request $request)
	{   
	    $validator = Validator::make($request->all(), [
			'city_id'         		=> 'required',
		]);
		if ($validator->fails()) {
			$re = [
				'status'    => false,
				'message'   => 'Validations errors found.',
				'errors'    => $validator->errors()
			];
		} else {
		$category = $id;

		

		$purchase_plan = PurchasePlan::where('status','Active')->get();
		// dd($purchase_plan);
		// $lists = Product::with('user','shop')->get();

		$listArr = [];

		foreach($purchase_plan as $pp){
			$lists = Product::whereHas('shop', function ($r) use ($request) {
				$r->where('city_id', '=', $request->city_id);
			})->where('shop_id',$pp->shop_id)->where('category_id',$category->id)->where('status',true)->with('category', 'user')->take($pp->plan->product_limit)->get()->toArray();
			
			// dd($lists);
			

			$listArr = array_merge($listArr, $lists);
		}

					
		$re = [
			'status' 	=> true,
			'data'   	=> $listArr
		];
		}

		return response()->json($re);
	}
}
