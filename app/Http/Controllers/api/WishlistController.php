<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Image;

use App\Model\Favorite;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class WishlistController extends Controller
{
	public function add_wishlist(Request $request){
	    
	    
	    $user_id                = auth()->user()->id;
	    
	    $validator = Validator::make($request->all(), [ 
            
            'product_id' 				=> 'required'
          
		]);
		
        if ($validator->fails()) {
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = $validator->errors(); 
		    $data['msg'] = $this->validationErrorsToString($validator->errors()); 
	        return response()->json($data, 200);      
		}
		
		$input = $request->all();
		$obj = new Favorite();
		$obj->product_id = $request->product_id;
		$obj->user_id    = $user_id;
		if($obj->save()){
		    $data = array();
			$data['status'] = 'true'; 
			$data['data'] = $obj; 
		    $data['msg'] = 'Product add successful';
		} else {
		    $data = array();
			$data['status'] = 'failed'; 
			$data['data'] = ''; 
		    $data['msg'] = 'Product add not success';
		}
		return response()->json($data, 200); 
	}
	public function remove_wishlist(Request $request){
	    $user_id                = auth()->user()->id;
	    $validator = Validator::make($request->all(), [ 
            
            'product_id' 				=> 'required'
            // 'favorite_id' 				=> 'required'
          
		]);
		
		
        if ($validator->fails()) {
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = $validator->errors(); 
		  //  $data['msg'] = $this->validationErrorsToString($validator->errors()); 
	        return response()->json($data, 200);      
		}
		
		$wish_count = Favorite::where('product_id',$request->product_id)->where('user_id',$user_id)->get();
		if(count($wish_count)){
		    $favorite = Favorite::where('product_id',$request->product_id)->where('user_id',$user_id)->delete();
    		
    		    $data = array();
    			$data['status'] = 'true'; 
    		    $data['msg'] = 'Product delete successful';
    	
		} else {
		        $data = array();
    			$data['status'] = false; 
    		    $data['msg'] = 'Record Not Found';
		}
		
		return response()->json($data, 200); 
	}
	public function show_wishlist(Request $request){
	    
	    $user_id                = auth()->user()->id;
		
	
          
              $favorite = DB::table('favorite_product')
                        ->select('favorite_product.*','products.*','favorite_product.id as favorite_id')
                        ->leftJoin('products','products.id','=','favorite_product.product_id')
                        ->where('user_id',$user_id)
                        ->get();
               
// 		dd($favorite);
		if(count($favorite) > 0){
		    $data = array();
			$data['status'] = true; 
			$data['data']  = $favorite;
		    $data['msg'] = count($favorite).' Wishlist products';
		    
		} else {
		    $data = array();
			$data['status'] = false; 
		    $data['msg'] = 'Record Not Found';
		}
		return response()->json($data, 200); 
	}
}