<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Pages;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function page($slug)
    {
        $list = Pages::where('slug', $slug)->whereHas('role', function ($q) {
            $q->where('name', 'Marchant');
        })->firstOrFail();

        return response()->json($list);
    }

    public function pages()
    {
        $lists = Pages::whereHas('role', function ($q) {
            $q->where('name', 'Marchant');
        })->get();

        $re = [
            'message'   => $lists->count() .' records found.',
            'data'      =>  $lists
        ];
        return response()->json($re);
    }
    public function userpage($slug)
    {
        $list = Pages::where('slug', $slug)->whereHas('role', function ($q) {
            $q->where('name', 'User');
        })->firstOrFail();

        return response()->json($list);
    }

    public function userpages()
    {
        $lists = Pages::whereHas('role', function ($q) {
            $q->where('name', 'User');
        })->get();

        $re = [
            'message'   => $lists->count() .' records found.',
            'data'      =>  $lists
        ];
        return response()->json($re);
    }
}
