<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\EditprofileRequest;
use App\Http\Requests\ChangepasswordRequest;
use App\Model\Shop;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Hash;
use Validator;

use App\User;

class UserloginController extends Controller
{
    public function login()
    {
        $user = User::where('mobile', request('mobile'))->whereHas('role', function ($q) {
            $q->where('name', 'User');
        });
        
        $users =  User::where('mobile', request('mobile'))->where('role_id',2)->first();
        
        if (empty(request('mobile') || empty(request('otp')))) {
            $re = [
                'status'    => false,
                'message'   => 'Required field(s) missing.',
                'isExists'  => 0,
            ];
            $code = 401;
        } elseif ($user->count() == 0) {
            $re = [
                'status'    => true,
                'message'   => 'User not registered',
                'isExists'  => 0,
            ];
            $code = 200;
        } elseif ($users->otp == request('otp')) {
            $obj =  User::where('mobile', request('mobile'))->where('role_id',2)->first();
            
            $obj->is_verified  = true;
            $obj->save();
            
            Auth::attempt(['mobile' => request('mobile'), 'password' => request('otp'), 'role_id' => '2']);
            $user   = Auth::user();
            // $input = [
            //     'device_type'   => request('device_type'),
            //     'device_id'     => request('device_id'),
            //     'fcm_id'        => request('fcm_id')
            // ];
            // dd($input);
            $user->device_type = request('device_type');
             $user->device_id = request('device_id');
              $user->fcm_id = request('fcm_id');

            $user->save();

            $token  = $user->createToken('SwarnManthan')->accessToken;
            $user_detail = DB::table('users')
                ->select('users.*', 'cities.name as city_name')
                ->leftjoin('cities', 'users.city_id', 'cities.id')
                ->where('users.id', '=', $obj->id)
                ->first();


            $re = [
                'status'    => true,
                'message'   => 'You\'ve logged in successfully.',
                'token'     => $token,
                'user_details' => $user_detail,
                'isExists'  => 1,
            ];
            $code = 200;
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Unauthorised! Credentials not matched.',
            ];
            $code = 401;
        }
        return response()->json($re, $code);
    }
    // public function login(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'mobile'     => 'required|string|regex:/\d{10}/',
    //     ]);
    //     if ($validator->fails()) {
    //         $re = [
    //             'status'    => false,
    //             'message'   => 'Validations errors found.',
    //             'errors'    => $validator->errors()
    //         ];
    //     } else {
    //         // Check if mobile number exists or not
    //         $user = User::where('mobile', $request->mobile)->whereHas('role', function ($q) {
    //             $q->where('name', 'User');
    //         })->first();
    //         if (!empty($user->id)) {
    //             if ($user->is_verified == 'true') {
    //                 $request->password = 'suncity';
    //                 // $credentials = $request->only('mobile', 'password');
    //                 $remember    = !empty($request->remember) ? true : false;

    //                 if (Auth::attempt(['mobile' => $request->mobile, 'password' => $request->password], $remember)) {
    //                     $user = Auth::user();
    //                     $input = [
    //                         'device_type'   => request('device_type'),
    //                         'device_id'     => request('device_id'),
    //                         'fcm_id'        => request('fcm_id')
    //                     ];

    //                     $user->fill($input)->save();

    //                     $token = $user->createToken('swarn-manthan')->accessToken;

    //                     $re = [
    //                         'status'    => true,
    //                         'message'   => 'Success!! Login successfully.',
    //                         'data'      => $user,
    //                         'token'     => $token,
    //                     ];
    //                 } else {
    //                     $re = [
    //                         'status'    => false,
    //                         'message'   => 'Error!! Credentials not matched.',
    //                     ];
    //                 }
    //             } else {
    //                 $re = [
    //                     'status'    => false,
    //                     'message'   => 'Error!! Mobile number not verified. please verify.',
    //                 ];
    //             }
    //         } else {
    //             $re = [
    //                 'status'    => false,
    //                 'message'   => 'Error!! Mobile not exists.',
    //             ];
    //         }
    //     }
    //     return response()->json($re);
    // }

    public function edit_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|string|regex:/\d{10}/',
            'email'   => 'required',
            'name'   => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->all();
            $user = auth()->user();
            if ($request->hasFile('image')) {
                $image        = $request->file('image');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(150, 175);
                $image_resize->save(public_path('imgs/' . $filename));
                $input['image']   = $filename;
            }
            if ($user->fill($input)->save()) {
                $re = [
                    'message' =>  'Updated Successfully'
                ];
                return response()->json($re);
            } else {
                $re = [
                    'message' => 'Please try again'
                ];
                return response()->json($re, 403);
            }
        }

        return response()->json($re);
    }

    public function change_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->all();

            if (Auth::Check()) {

                $current_password = Auth::User()->password;
                if (password_verify($input['current_password'], $current_password)) {

                    $user_id = Auth::User()->id;
                    $obj_user = User::find($user_id);
                    $obj_user->password = bcrypt($input['new_password']);
                    $obj_user->save();
                    // return response()->json(['success'=>'Password Changed Successfully'], $this-> successStatus); 

                    $result = array();
                    $result['status'] = 'success';
                    $result['data'] = [];
                    $result['msg'] = 'Password Changed Successfully';
                    return response()->json($result);
                } else {
                    $result = array();
                    $result['status'] = 'failed';
                    $result['data'] = [];
                    $result['msg'] = 'Please enter correct current password';
                    return response()->json($result, 200);
                }
            } else {
                $result = array();
                $result['status'] = 'failed';
                $result['data'] = [];
                $result['msg'] = 'Incorrect old Password';
                return response()->json($result, 200);
                //return response()->json(['error'=>'Incorrect old Password'], 200); 
            }
        }
        return response()->json($re, 200);
    }

    public function logout()
    {
        $user = auth()->user();

        $user->device_id = '';
        $user->save();
        $re = [
            'status'    => true,
            'message'   => 'Success! You are logout successfully.',
        ];

        return response()->json($re);
    }
}
