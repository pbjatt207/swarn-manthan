<?php

namespace App\Http\Controllers;

use App\Model\BusinessEnquery;
use Illuminate\Http\Request;
use App\Model\Marchant;
use App\Model\Setting;
use App\Model\City;
use App\Model\Staff;
use App\Model\Shop;
use App\Model\Plan;
use App\Model\PurchasePlan;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller as BaseController;



class MarchantController extends BaseController
{
    public function index($id)
    {
        
        
        $setting = Setting::find(1);
        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach ($city as $c) {
            $cityArr[$c->id] = $c->name;
        }

        $data  = compact('cityArr','setting','id');
        return view('frontend.inc.add_marchant', $data);
    }
    
    public function add_marchant(Request $request, $id)
    {
       
        $rules = [
            'record.name'         => 'required',
            'mobile'              => 'required|unique:users',
            'email'               => 'required|unique:users',
            'record1.city_id'     => 'required',
            'record1.name'         => 'required',
            'record1.pincode'      => 'required',
            'record1.address'      => 'required',
        ];


        $request->validate($rules);
        $input = $request->record;
        // dd($input);
        $input['role_id'] =  '3';
        $input['mobile'] =  $request->mobile;
        $input['email'] =  $request->email;
        // $input['password'] =  Hash::make($request->record['password']);
        
        $obj = new User($input);

        $obj->save();

        $input1 = $request->record1;
        $input1['user_id'] =  $obj->id;
        $input1['slug']  = Str::slug($input1['name'].'-');
        $input1['staff_id'] = $id;

        $obj1 = new Shop($input1);

        $obj1->save();
        $shop = Shop::where('id',$obj1->id)->update(['slug' => $obj1->slug.'-'.$obj1->id]);

        

                
        $plan_detail = Plan::find($request->record2['plan_id']);
        
                
        if($plan_detail->time_limit_in == 'Day'){
            $expiry_date = Carbon::now()->addDays($plan_detail->time_limit);
        }
        if($plan_detail->time_limit_in == 'Month'){
            $expiry_date = Carbon::now()->addMonths($plan_detail->time_limit);
        }
        if($plan_detail->time_limit_in == 'Year'){
            $expiry_date = Carbon::now()->addYears($plan_detail->time_limit);
        }
        
        // $input                      = $request->all();
        $plan_rec['shop_id']           = $obj1->id;
        $plan_rec['plan_id']           = $plan_detail->id;
        $plan_rec['payment']           = $plan_detail->price;
        $plan_rec['subscription_date'] = Carbon::now();
        $plan_rec['expiry_date']       = $expiry_date;
        
        $plan_obj = new PurchasePlan();
        $plan_obj->fill($plan_rec);
        // $plan_obj->save();

        $plan_obj->save();
        

            
        // $shop_slug = Str::slug($shop->name.'-')
        // $shop->update(['slug' => $shop->slug.'-'.$shop->id]);

        return redirect()->back()->with('success', 'Success! New record has been added.');
        
    }
}
