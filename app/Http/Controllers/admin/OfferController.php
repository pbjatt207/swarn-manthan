<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Model\Offer;
use Illuminate\Http\Request;
use Image;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Offer::orderBy('id', 'desc')->paginate(10);
        $page  = 'offer.list';
        $title = 'Offer list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page  = 'offer.add';
        $title = 'Add Offer';
        $data  = compact('page', 'title');
        return view('admin.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'        => 'required',
            'description'        => 'required',
        ];


        $request->validate($rules);
        $input = $request->all();


        $obj = new Offer($input);

        if ($file = $request->file('image')) {

            $path = public_path() . '/imgs/offer/';
            if (!file_exists($path)) {
                mkdir($path, 0775, true);
            }

            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/imgs/offer/';
            $filename = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $filename, 72);

            $obj->image = $filename;
        }

        $obj->slug = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);

        $obj->save();

        return redirect(url('admin/offer'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Offer  $Offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Offer  $Offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer, Request $request)
    {
        $edit = $offer;
        $request->replace($edit->toArray());
        $request->flash();
        $page  = 'offer.edit';
        $title = 'Offer Edit';
        $data  = compact('page', 'title', 'edit', 'request');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Offer  $Offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        $rules = [
            'title'        => 'required',
            'description'        => 'required',
        ];


        $request->validate($rules);
        $obj = $offer;
        $obj->title = $request->title;

        if ($file = $request->file('image')) {

            $path = public_path() . '/imgs/offer/';
            if (!file_exists($path)) {
                mkdir($path, 0775, true);
            }

            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/imgs/offer/';
            $filename = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $filename, 72);

            $obj->image = $filename;
        }

        $obj->slug = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->description = $request->description;
        $obj->update();

        return redirect(url('admin/offer'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Offer  $Offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {

        $ids = $request->sub_chk;
        // dd($ids);
        Offer::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }
}
