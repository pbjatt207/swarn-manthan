<?php

namespace App\Http\Controllers\admin;

use App\Model\BusinessEnquery;
use App\Model\Category;
use App\Model\ContactEnquery;
use App\Model\Enquery;
use App\Model\Product;
use App\Model\Shop;
use Illuminate\Routing\Controller as BaseController;
use App\User;

class DashboardController extends BaseController
{
    public function index()
    {
        $page = 'dashboard';
        $title = 'Master Admin Dashboard';
        $shop  = Shop::count();
        $user  = User::count();
        $category  = Category::count();
        $product  = Product::count();

        $enquery = Enquery::count();
        $contact_enquery = ContactEnquery::count();
        $business_enquery = BusinessEnquery::count();

        $data = compact('page', 'title', 'shop', 'category', 'product', 'user', 'enquery', 'contact_enquery', 'business_enquery');

        return view('admin.layout', $data);
    }
}
