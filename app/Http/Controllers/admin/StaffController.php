<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Role;
use App\Model\Staff;
use Illuminate\Support\Str;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Staff::with('city')->get();

        // dd($lists);
        $page  = 'staff.list';
        $title = 'Staff list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }
    public function userlist()
    {
        $lists = Staff::with('role')->where('role_id', 2)->get();
        // dd($lists);
        $page  = 'user.user_list';
        $title = 'Staff list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page  = "staff.add";
        $title = "Staff Add";

        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach ($city as $c) {
            $cityArr[$c->id] = $c->name;
        }
        $edit = '';

        $data  = compact('page', 'title', 'request', 'cityArr', 'edit');
        return view('admin.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'         => 'required',
            'mobile'       => 'required|unique:staffs',
            'email'        => 'required|unique:staffs',
            'city_id'      => 'required',
        ];


        $request->validate($rules);
        $input = $request->all();
        // $input['role_id'] =  '4';

        if($request->hasFile('aadhar_photo'))  { 
            
            $image       = $request->file('aadhar_photo');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(750, 500);
            // dd($filename);
            $image_resize->save(public_path('imgs/staff/' .$filename));
            $input['aadhar_photo']   = $filename;
            
        }

        $obj = new Staff($input);

        $obj->save();

        // $input1 = $request->record1;
        // $input1['user_id'] =  $obj->id;
        // $input1['slug']  = Str::slug($input1['name'].'-');

        // $obj1 = new Shop($input1);

        // $obj1->save();
        // $shop = Shop::where('id',$obj1->id)->update(['slug' => $obj1->slug.'-'.$obj1->id]);
        // $shop_slug = Str::slug($shop->name.'-')
        // $shop->update(['slug' => $shop->slug.'-'.$shop->id]);

        return redirect(route('staff.index'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Staff $staff)
    {
        
        $edit = Staff::findOrFail($staff->id);
        $request->replace($edit->toArray());
        $request->flash();

        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach ($city as $c) {
            $cityArr[$c->id] = $c->name;
        }

        $page  = 'staff.edit';
        $title = 'Staff Edit';
        $data  = compact('page', 'title', 'edit', 'request', 'cityArr');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff)
    {
        // dd($staff);
        $rules = [
            'name'         => 'required',
            'email'        => 'required|unique:staffs,email,' . $staff->id,
            'mobile'       => 'required|unique:staffs,mobile,' . $staff->id,
            'city_id'      => 'required',
        ];
        $request->validate($rules);

        $obj =  Staff::findOrFail($staff->id);
        $input = $request->all();

        if($request->hasFile('aadhar_photo'))  { 
            
            $image       = $request->file('aadhar_photo');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(750, 500);
            // dd($filename);
            $image_resize->save(public_path('imgs/staff/' .$filename));
            $input['aadhar_photo']   = $filename;
            
        }
        
       
        $obj->update($input);

        

        return redirect(route('staff.index'))->with('success', 'Success!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $user)
    {
        $user->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        // dd($request);
        $ids = $request->sub_chk;
        // dd($ids);
        Staff::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function changestatus(Request $request, Staff $user)
    {
        // dd($user);
        $user->status = $request->status;
        $user->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
