<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\Role;
use App\Model\Plan;
use App\Model\PurchasePlan;
use App\Model\Shop;
use Illuminate\Support\Str;
use DateTime;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        

        $query = User::with('role')->latest();

        if( !empty( $request->search ) ) {
            $query->orWhereHas('shop_name', function($q) use ($request){
                $q->where('name', 'LIKE', '%'.$request->search.'%');
            });
            $query->orWhere(function ($q) use ($request) {
                
                $q->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('email', 'like', '%' . $request->search . '%')
                ->orWhere('mobile', 'like', '%' . $request->search . '%');
            });
            
            // $query->where('name', 'LIKE', '%'.$request->name.'%');
        }
        $lists = $query->where('role_id',3)->paginate(10);

        foreach($lists as $list){
            $dt = new DateTime();
            $dt = $dt->format('Y-m-d');     

            $purchase_plan = PurchasePlan::where('shop_id',$list->shop_name->id)->first();
            $list->purchase_plan = $purchase_plan ?? 'null';
            
            // dd($list->purchase_plan->plan->name);
        }
        $page  = 'user.list';
        $title = 'Marchant list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }
    public function userlist()
    {
        $lists = User::with('role')->where('role_id', 2)->get();
        // dd($lists);
        $page  = 'user.user_list';
        $title = 'User list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page  = "user.add";
        $title = "Marchant Add";

        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach ($city as $c) {
            $cityArr[$c->id] = $c->name;
        }
        $plan = Plan::where('type','marchant')->get();
        $planArr = [
            '' => 'Select Plan'
        ];
        foreach ($plan as $c) {
            $planArr[$c->id] = $c->name;
        }
        $edit = '';

        $data  = compact('page', 'title', 'request', 'cityArr', 'edit', 'planArr');
        return view('admin.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record.name'         => 'required',
            'mobile'       => 'required|unique:users',
            'email'        => 'required|unique:users',
            'record1.city_id'      => 'required',
            'record1.name'      => 'required',
            'record1.pincode'      => 'required',
            'record1.address'      => 'required',
        ];


        $request->validate($rules);
        $input = $request->record;
        $input['role_id'] =  '3';
        $input['mobile'] =  $request->mobile;
        $input['email'] =  $request->email;
        // $input['password'] =  Hash::make($request->record['password']);

        $obj = new User($input);

        $obj->save();

        $input1 = $request->record1;
        $input1['user_id'] =  $obj->id;
        $input1['slug']  = Str::slug($input1['name'].'-');

        $obj1 = new Shop($input1);

        $obj1->save();
        $shop = Shop::where('id',$obj1->id)->update(['slug' => $obj1->slug.'-'.$obj1->id]);

        $plan_detail = Plan::find($request->record2['plan_id']);
        
                
        if($plan_detail->time_limit_in == 'Day'){
            $expiry_date = Carbon::now()->addDays($plan_detail->time_limit);
        }
        if($plan_detail->time_limit_in == 'Month'){
            $expiry_date = Carbon::now()->addMonths($plan_detail->time_limit);
        }
        if($plan_detail->time_limit_in == 'Year'){
            $expiry_date = Carbon::now()->addYears($plan_detail->time_limit);
        }
        
        // $input                      = $request->all();
        $plan_rec['shop_id']           = $obj1->id;
        $plan_rec['plan_id']           = $plan_detail->id;
        $plan_rec['payment']           = $plan_detail->price;
        $plan_rec['subscription_date'] = Carbon::now();
        $plan_rec['expiry_date']       = $expiry_date;
        
        $plan_obj = new PurchasePlan();
        $plan_obj->fill($plan_rec);
        // $plan_obj->save();

        $plan_obj->save();
        // $shop_slug = Str::slug($shop->name.'-')
        // $shop->update(['slug' => $shop->slug.'-'.$shop->id]);

        return redirect(route('marchant.index'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $marchant)
    {
        $edit = User::findOrFail($marchant->id);
        $shop = Shop::where('user_id', $marchant->id)->first();
        $request->replace($edit->toArray());
        $editData =  ['record' => $edit->toArray(), 'record1' => $shop->toArray()];
        $request->replace($editData);
        $request->flash();
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');
        

        $purchase_plan = PurchasePlan::where('shop_id',$shop->id)->where('expiry_date','>',$dt)->first();
        // dd($purchase_plan);

        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach ($city as $c) {
            $cityArr[$c->id] = $c->name;
        }
        $plan = Plan::where('type','marchant')->get();
        $planArr = [
            '' => 'Select Plan'
        ];
        foreach ($plan as $c) {
            $planArr[$c->id] = $c->name;
        }

        $page  = 'user.edit';
        $title = 'User Edit';
        $data  = compact('page', 'title', 'edit', 'request', 'cityArr', 'planArr','purchase_plan');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $marchant)
    {
        // dd($marchant);
        $rules = [
            'record.name'         => 'required',
            'email'        => 'required|unique:users,email,' . $marchant->id,
            'mobile'       => 'required|unique:users,mobile,' . $marchant->id,
            'record1.city_id'      => 'required',
            'record1.name'      => 'required',
            'record1.pincode'      => 'required',
            'record1.address'      => 'required',
        ];
        $request->validate($rules);

        $obj =  User::findOrFail($marchant->id);
        $input = $request->record;
        // if($request->record['password'] != '' || $request->record['password'] != null) {
        //     $input['password'] =  Hash::make($request->record['password']);
        // }else {
        //     $input['password'] =  $marchant->password;
        // }
        $input['mobile'] =  $request->mobile;
        $input['email'] =  $request->email;
        $obj->update($input);

        $obj1 = Shop::where('user_id', $marchant->id)->first();
        $input1 = $request->record1;
        $input1['slug'] = Str::slug($input1['name'].'-'.$obj1->id.'-');
        $obj1->update($input1);
        if($request->record2){
            $fetch_shop = PurchasePlan::where('shop_id',$obj1->id)->first();
            

            $plan_detail = Plan::find($request->record2['plan_id']);
            
                    
            if($plan_detail->time_limit_in == 'Day'){
                $expiry_date = Carbon::now()->addDays($plan_detail->time_limit);
            }
            if($plan_detail->time_limit_in == 'Month'){
                $expiry_date = Carbon::now()->addMonths($plan_detail->time_limit);
            }
            if($plan_detail->time_limit_in == 'Year'){
                $expiry_date = Carbon::now()->addYears($plan_detail->time_limit);
            }
            
            // $input                      = $request->all();
            $plan_rec['shop_id']           = $obj1->id;
            $plan_rec['plan_id']           = $plan_detail->id;
            $plan_rec['payment']           = $plan_detail->price;
            $plan_rec['subscription_date'] = Carbon::now();
            $plan_rec['expiry_date']       = $expiry_date;
            $plan_rec['status']       = 'Active';
            
            
            if($fetch_shop){
                $fetch_shop->fill($plan_rec);            
    
                $fetch_shop->save();
            } else {

                $plan_obj = new PurchasePlan();
                $plan_obj->fill($plan_rec);
                // $plan_obj->save();
        
                $plan_obj->save();
            }
        }


        

        return redirect(route('marchant.index'))->with('success', 'Success!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        // dd($request);
        $ids = $request->sub_chk;
        // dd($ids);
        User::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function changestatus(Request $request, User $user)
    {
        // dd($user);
        $user->status = $request->status;
        $user->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
