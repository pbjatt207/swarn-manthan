<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Plan;
use App\Model\PurchasePlan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Plan::latest()->paginate(10);
        $page  = 'plan.add_plan';
        $title = 'Add Plan';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'  => 'required',
            'record.price'  => 'required',
        ];
        
        
        $messages = [
            'record.name.required'  => 'Please Enter Plan Name.',
            'record.price.required'  => 'Please Enter Price.'
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Plan;
        $input            = $request->record;
        
        // $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-'):$input['slug'];
        $record->fill($input);
        
        if ($record->save()) {
            return redirect(url('admin/plan'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url('admin/plan'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(Plan $plan)
    {
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $edit     =  Plan::find($id);
        
        $editData =  ['record'=>$edit->toArray()];

        $request->replace($editData);
        //send to view
        $request->flash();
        
        // set page and title ------------------
        $page = 'plan.edit';
        $title = 'Edit Plan';
        $data = compact('page', 'title','id', 'edit');
        // return data to view

        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record           = Plan::find($id);
        $input            = $request->record;
        // $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-'):$input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(url('admin/plan'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $row)
    {
        $row->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        // dd($ids);
        Plan::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function purchase_plan()
    {
        $lists = PurchasePlan::latest()->paginate(10);
        $page  = 'plan.purchase_plan';
        $title = 'Purchase Plan';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('admin.layout', $data);
    }

    public function purchaseplanchangestatus(Request $request, PurchasePlan $user)
    {
        // dd($user);
        $user->status = $request->status;
        $user->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
