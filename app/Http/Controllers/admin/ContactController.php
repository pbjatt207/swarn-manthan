<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\BusinessEnquery;
use App\Model\ContactEnquery;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $lists = ContactEnquery::get();
        $page  = 'contact.list';
        $title = 'Contact Enquery';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        ContactEnquery::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function changestatus(Request $request, $id)
    {
        $obj = ContactEnquery::findOrFail($id);
        $obj->status = $request->status;
        $obj->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function business_index()
    {
        $lists = BusinessEnquery::get();
        $page  = 'contact.business';
        $title = 'Business Enquery';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }

    public function business_destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        BusinessEnquery::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function business_changestatus(Request $request, $id)
    {
        $obj = BusinessEnquery::findOrFail($id);
        $obj->status = $request->status;
        $obj->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
