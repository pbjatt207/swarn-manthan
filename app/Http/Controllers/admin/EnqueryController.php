<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Enquery;
use App\Model\EnqueryProduct;
use Illuminate\Http\Request;

class EnqueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function marchantEnquiry(Request $request)
    {

        $input = $request->all();
        // dd();
        // 			$order_trans = Order_trans::with('coupon','store','user')->whereDate('created_at',$input['date'])->get();

        $tasks = EnqueryProduct::with('enquery');
        if (!empty($input['date'])) {

            $tasks->whereDate('created_at', 'LIKE', '%' . $input['date'] . '%');
        }
        if (!empty($input['shop'])) {
            $tasks->whereHas('enquery', function ($q) use ($input) {

                $q->where('shop_id', 'LIKE', '%' . $input['shop'] . '%');
            });
        }

        $lists = $tasks->get();
        $page  = 'enquiry.marchant';
        $title = 'Marchant Enquiry list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }
    public function userEnquiry(Request $request)
    {

        $input = $request->all();
        // dd();
        // 			$order_trans = Order_trans::with('coupon','store','user')->whereDate('created_at',$input['date'])->get();

        $tasks = EnqueryProduct::with('enquery');
        if (!empty($input['date'])) {

            $tasks->whereDate('created_at', 'LIKE', '%' . $input['date'] . '%');
        }
        if (!empty($input['user'])) {
            $tasks->whereHas('enquery', function ($q) use ($input) {

                $q->where('user_id', 'LIKE', '%' . $input['user'] . '%');
            });
        }

        $lists = $tasks->get();
        $page  = 'enquiry.user';
        $title = 'User Enquiry list';
        $data  = compact('lists', 'page', 'title');
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enquery  $enquery
     * @return \Illuminate\Http\Response
     */
    public function show(Enquery $enquery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enquery  $enquery
     * @return \Illuminate\Http\Response
     */
    public function edit(Enquery $enquery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enquery  $enquery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enquery $enquery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enquery  $enquery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enquery $enquery)
    {
        //
    }
}
