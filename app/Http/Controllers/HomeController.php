<?php

namespace App\Http\Controllers;

use App\Model\BusinessEnquery;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\City;
use App\Model\ContactEnquery;
use App\Model\Setting;
use App\Model\Pages;
use App\Model\Product;
use App\Model\Role;
use App\Model\Shop;
use Illuminate\Routing\Controller as BaseController;



class HomeController extends BaseController
{
    public function index()
    {
        $setting = Setting::find(1);
        $categories = Category::get();
        
        
        $data = compact('setting', 'categories');
        return view('frontend.inc.homepage', $data);
    }
    public function page($slug)
    {
        
        $setting = Setting::find(1);
        
        $page = Pages::where('slug', $slug)->where('role_id', 1)->firstOrFail();
        
        $data = compact('page', 'setting');
        return view('frontend.inc.page', $data);
    }

    public function otherpage($role, $slug)
    {
        $setting = Setting::find(1);
        $roles = Role::where('name', $role)->first();
        $page = Pages::where('slug', $slug)->where('role_id', $roles->id)->firstOrFail();
        $data = compact('page', 'setting');
        return view('frontend.inc.page', $data);
    }

    public function contact()
    {
        $setting = Setting::find(1);
        $data = compact('setting');
        return view('frontend.inc.contact', $data);
    }

    public function ourpartner(Request $request)
    {
        $setting = Setting::find(1);
        $city = $request->city;
        
        $query = Shop::whereHas('user', function ($q) {
            $q->where('status', '=', 'enable');
        });
        
        if ($city) {
            // $query->where('city_id', $city);
            $query->whereHas('city', function ($r) use ($city) {
                $r->where('name',  $city);
            });
        }
        $marchants = $query->get();

        $cities = City::get();
        $cityArr  = ['' => 'All'];
        if (!$cities->isEmpty()) {
            foreach ($cities as $mcat) {
                $cityArr[$mcat->name] = $mcat->name;
            }
        }

        $data = compact('marchants', 'city', 'cityArr', 'setting');
        return view('frontend.inc.ourpartner', $data);
    }

    public function contact_enquery(Request $request)
    {
        $input = $request->all();

        $obj = new ContactEnquery($input);
        $obj->save();

        return redirect(route('contact'))->with('success', 'Success! Enquery has been send successfully.');
    }

    public function business_enquery()
    {

        $city = City::get();
        $cityArr = [
            '' => 'Select City'
        ];
        foreach ($city as $c) {
            $cityArr[$c->id] = $c->name;
        }
        $setting = Setting::find(1);
        $data = compact('setting', 'cityArr');
        return view('frontend.inc.business', $data);
    }

    public function business_enquery_post(Request $request)
    {
        $input = $request->all();

        $obj = new BusinessEnquery($input);
        $obj->save();

        return redirect(route('business'))->with('success', 'Success! Enquery has been send successfully.');
    }
    public function products(Request $request, $slug){
        $shop = Shop::where('slug',$slug)->first();
        $list = Product::where('shop_id',$shop->id)->get();
        // dd($list);
        $setting = Setting::find(1);
        $data = compact('list','setting','shop');
        return view('frontend.inc.products', $data);
    }
}
