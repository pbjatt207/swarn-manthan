<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BusinessEnquery extends Model
{
    protected $guarded  = [];
    protected $with  = ['city_name'];

    public function city_name()
    {
        return $this->hasOne('App\Model\City', 'id', 'city');
    }
}
