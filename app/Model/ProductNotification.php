<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductNotification extends Model
{
    protected $table = 'product_notification';
    protected $guarded  = [];

}
