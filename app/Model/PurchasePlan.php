<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PurchasePlan extends Model
{
    //
    protected $guarded = [];

    protected $with = ['plan', 'shop'];

    public function plan()
    {
        return $this->hasOne('App\Model\Plan', 'id', 'plan_id');
    }
    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
}
