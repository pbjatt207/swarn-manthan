<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $guarded  = [];

    protected $table = 'favorite_product';
}
