<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded  = [];

    protected $with = ['user', 'shop', 'product'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
    public function product()
    {
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
}
