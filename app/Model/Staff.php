<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table  = 'staffs';
    protected $guarded  = [];

    public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
    }
}
