@php
$setting = App\Model\Setting::find(1);
@endphp
<!DOCTYPE html>
<html>

<head>
	<base href="{{ url('/') }}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<meta name="author" content="Shrawan Choudhary" /0
	<link rel="icon" type="image/icon" href="{{ url('imgs/favicon/'.$setting->favicon) }}">
	<meta name="_token" content="{{ csrf_token() }}">
	<title> @yield('title') | {{ $setting->title }}</title>

	<!-- theme styles -->
	<link href="{{ url('public/web/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- bootstrap css -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/fontawesome/css/fontawesome-all.min.css') }}" />
	<!-- fontawesome css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/css/font-awesome.min.css') }}" />
	<!-- fontawesome css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/flaticon/flaticon.css') }}" /> <!-- flaticon css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/owl/css/owl.carousel.min.css') }}" />
	<!-- owl carousel css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/datatables/css/responsive.datatables.min.css') }}" />
	<!-- datatables responsive -->
	<link href="{{ url('public/web/css/jquery.rateyo.css') }}" rel="stylesheet" type="text/css" />
	<!-- rateyo css -->
	<link href="{{ url('public/web/vendor/datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
	<!-- datepicker css -->
	<link rel="stylesheet" type="text/css" href="../cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css" /> <!-- summernote css -->
	<link href="{{ url('public/web/css/summernote-bs4.css') }}" rel="stylesheet" type="text/css" />
	<!-- summernote css -->
	<link href="{{ url('public/web/css/select2.css') }}" rel="stylesheet" type="text/css" />
	<!-- select css -->
	<link href="{{ url('public/web/css/style.css') }}" rel="stylesheet" type="text/css" />

</head>

<body>
	<div>
		@section('header')
		<!-- <div class="modal" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Download App</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<p class="text-center">
						<div class='row'>
							<div class="app-badge play-badge col-sm-6">
								<a href="#" target="_blank" title="Google Play">
									<img style='height:80px' src="{{ url('web/images/google-play.png') }}" class="img-fluid" alt="Google Play">
								</a>
							</div>
							<div class="app-badge col-sm-6">
								<a href="{{ url('/') }}" target="_blank" title="Apple App Store">
									<img style='height:80px' src="{{ url('web/images/app-store.png') }}" class="img-fluid" alt="Apple App Store">
								</a>
							</div>
						</div>
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div> -->
		<section class="logo-block p-0">
			<div class="container">
				<div class="row">
					<div class="col-3 col-lg-3 col-md-3 pr-0">
						<div class="logo">
							<a href="{{ route('home') }}" title="Home">
								<img src="{{ url('public/imgs/logo/'.$setting->logo) }}" class="img-fluid" alt="Logo">
							</a>
						</div>
					</div>
					<div class="col-9 col-lg-9 col-md-9 pl-0 menu">
						<div class="text-right bar-icon">
							<span class="navbar-toggler-icon menuOpen"><i class="fas fa-bars"></i></span>
						</div>
						<div class="text-right bar-close-icon disable">
							<span class="navbar-toggler-icon menuClose"><i class="fas fa-close"></i></span>
						</div>
						<div class="menu-bar">
							<div class="row">
							    <div class="col-md-2 menu-link">
								</div>
								<div class="col-md-2 menu-link">
									<a class="" href="{{ route('home') }}">Home</a>
								</div>
								<div class="col-md-2 menu-link">
									<a class="" href="{{ route('page','about-us') }}">About Us</a>
								</div>
								<div class="col-md-2 menu-link">
									<a class="" href="{{ route('ourpartner') }}">Our Partners</a>
								</div>
								<div class="col-md-2 menu-link">
									<a class="" href="{{ route('contact') }}">Contact Us</a>
								</div>
								<div class="col-md-2 menu-link">
									<a class="" href="{{ route('page','term-and-condition') }}">T&C</a>
								</div>
								<!--<div class="col-md-2 menu-link">-->
								<!--	<a class="" href="{{ route('page','faq') }}">FAQ</a>-->
								<!--</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header-bottom-border"></div>
			<div class="clearfix"></div>
		</section>
		<!-- <div style="position: relative;"></div> -->
		<!-- 
		<div id="site-header" style="position: sticky; top: 0; z-index: 9999999; background: #fff;  box-shadow: 1px 0px 15px #000;"">
			<section class=" navbar mb-0 pb-0">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 pr-0">
						<div class="logo">
							<a href="{{ url('/') }}" title="Home">
								<img src="{{ url('imgs/logo/SwarnManthanWhite1024.png') }}" style="height: auto; width: 30%;" class="img-fluid" alt="Logo">
							</a>
						</div>
					</div>
					<div class="col-lg-9">
						<nav class="navbar navbar-expand-lg mb-0">
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="position: absolute; top: 0;font-size: 25px;-webkit-box-shadow: 0px 6px 10px -5px rgba(0,0,0,0.75);">
								<span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
							</button>
							<div class="collapse navbar-collapse pl-0 pr-0 mr-0" id="navbarSupportedContent">
								<ul class="navbar-nav p-0 m-0">
									<li class="nav-item text-center">
										<a class="nav-link active" href="{{ route('home') }}">Home</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="{{ route('page','about-us') }}">About Us</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="{{ route('page','term&condition') }}">Terms & Conditions</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="{{ route('page','faq') }}">FAQ</a>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
			</section>
		</div> -->
		@show

		<!-- <div class="overlay"></div> -->
		<div class="">
			@yield('contant')
		</div>

		@section('footer')

		<!-- <div id="back-what-app">
			<a title="what app" href="https://api.whatsapp.com/send?phone=+91-9116673793&amp;text=hello" target="_blank">
			<i class="fa fa-whatsapp" aria-hidden="true"></i>
		</a>
	</div> -->
		@php
		$company_page = App\Model\Pages::where('role_id',1)->get();
		$marchant_page = App\Model\Pages::where('role_id',3)->get();
		$user_page = App\Model\Pages::where('role_id',2)->get();
		@endphp
		<footer id="footer" class="footer-main-block">
			<div style="height: 0px">
				<a id="back2Top" title="Back to top" href="#">&#10148;</a>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">Company</h6>
							<ul>
								@foreach($company_page as $page)
								<li>
									<a href="{{ route('other-pages',['Admin',$page->slug]) }}" title="{{ $page->title }}">{{ $page->title }}</a>
								</li>
								@endforeach
								<li>
									<a href="{{ route('business') }}" title="Business Enquery">Business Enquery</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">Marchant</h6>
							<ul>
								@foreach($marchant_page as $page)
								<li>
									<a href="{{ route('other-pages',['Marchant',$page->slug]) }}" title="{{ $page->title }}">{{ $page->title }}</a>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">User</h6>
							<ul>
								@foreach($user_page as $page)
								<li>
									<a href="{{ route('other-pages',['User',$page->slug]) }}" title="{{ $page->title }}">{{ $page->title }}</a>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget footer-subscribe">
							<h6 class="footer-widget-heading">Head Office</h6>
							<div style="color: white;">
								{{ $setting->address }}
							</div>
							<div class="row">
								@if($setting->marchant_applink)
								<div class="col-6 col-lg-6 app-badge play-badge mt-3">
									<a href="{{ $setting->marchant_applink }}" target="_blank" title="Google Play"><img src="{{ url('public/web/images/google-play.png') }}" class="img-fluid" alt="Google Play"></a>
									<div class="text-center pt-2" style="font-weight: bold;">Marchant App</div>
								</div>
								@endif
								@if($setting->user_applink)
								<div class="col-6 col-lg-6 app-badge play-badge mt-3">
									<a href="{{ $setting->user_applink }}" target="_blank" title="Google Play"><img src="{{ url('public/web/images/google-play.png') }}" class="img-fluid" alt="Google Play"></a>
									<div class="text-center pt-2" style="font-weight: bold;">User App</div>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="border-divider">
			</div>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="copyright-text">
								<p style='color:black;'>&copy; 2021<a style='color:black;' href="{{ route('home') }}" title="{{ $setting->title }}"> {{ $setting->title }}</a> | All Rights Reserved.</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="social-icon">
								<ul>
									@if($setting->facebook)
									<li class="facebook-icon"><a href="{{ $setting->facebook }}" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
									@endif
									@if($setting->instagram)
									<li class="instagram-icon"><a href="{{ $setting->instagram }}" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a></li>
									@endif
									@if($setting->linkedin)
									<li class="linkedin-icon"><a href="{{ $setting->linkedin }}" target="_blank" title="linkedin"><i class="fa fa-linkedin"></i></a></li>
									@endif
									@if($setting->twitter)
									<li class="twitter-icon"><a href="{{ $setting->twitter }}" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a></li>
									@endif
									@if($setting->youtube)
									<li class="youtube-icon"><a href="{{ $setting->youtube }}" target="_blank" title="youtube"><i class="fa fa-youtube"></i></a></li>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		@show

	</div>

	{{ HTML::script('public/assets/vendor/jquery/jquery.min.js') }}
	{{ HTML::script('public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}
	{{ HTML::script('public/assets/vendor/jquery-easing/jquery.easing.min.js') }}
	{{ HTML::script('public/assets/js/validation.js') }}
	<script>
		$('#cityName').change(function() {
			$('#cityForm').submit();
		});
		$('.menuOpen').click(function() {
			$('.menu-bar').addClass('active');
			$('.bar-close-icon').addClass('active');
			$('.bar-close-icon').removeClass('disable');
		});
		$('.menuClose').click(function() {
			$('.menu-bar').removeClass('active');
			$('.bar-close-icon').removeClass('active');
			$('.bar-close-icon').addClass('disable');
		});
	</script>
</body>

</html>