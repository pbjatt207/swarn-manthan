@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('{{ url('public/imgs/headerimage/'.$setting->header_image) }}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">Contact Us</h2>
        </div>
    </div>
</div>
<section class="contact-section">
    <div class="container">
        @if (\Session::has('success'))
        <div class="alert alert-success toast-msg" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
        @endif

        @if (\Session::has('danger'))
        <div class="alert alert-danger toast-msg" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
        @endif
        <div class="row">
            <div class="col-lg-6 col-12 contact-info margin-btm-mobile">
                {!! Form::open(['method' => 'POST', 'route'=>array('contact-us'), 'class' => 'contact-form mb-5']) !!}
                @csrf
                <div class="form-group">
                    {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Your name','required'=>'required'])}}
                </div>
                <div class="form-group">
                    {{Form::text('email', '', ['class' => 'form-control', 'placeholder'=>'Your email','required'=>'required'])}}
                </div>
                <div class="form-group">
                    {{Form::text('mobile', '', ['class' => 'form-control', 'placeholder'=>'Your mobile','required'=>'required'])}}
                </div>
                <div class="form-group">
                    {{Form::text('subject', '', ['class' => 'form-control', 'placeholder'=>'Your subject','required'=>'required'])}}
                </div>
                <div class="form-group">
                    {{Form::textarea('message', '', ['class' => 'form-control', 'placeholder'=>'Your subject','style'=>'padding:10px; height:100px;','required'=>'required'])}}
                </div>
                <button type="submit" class="site-btn float-right">SEND NOW</button>
                {{ Form::close() }}
            </div>
            <div class="col-lg-6 col-12">
                <div class="map"><iframe src="{{ $setting->google_map }}" style="border:0" allowfullscreen=""></iframe></div>
            </div>
        </div>
    </div>
</section>

@endsection