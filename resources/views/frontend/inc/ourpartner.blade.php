@extends('frontend.layout.master')
@section('title','Our Partners')
@section('contant')
<div class="forum-page-header mb-5" style="background: url('{{ url('public/imgs/headerimage/'.$setting->header_image) }}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">Our Partners</h2>
        </div>
    </div>
</div>
<section class="section">
    <div class="container">
        <style>
            .padding-0 {
                padding-right: 0;
                padding-left: 0;
            }

            .card:hover .card-content a .card-category {
                margin-top: 40px;
            }
        </style>
        <div class="blog-page-main-block">
            <div class="blog-post-main">
                <form action="" method="get" id="cityForm">
                    <div class="">
                        <div class="form-group float-right" style="width: 30%;">
                            {{Form::select('city', $cityArr,$city, ['class' => 'form-control','id'=> 'cityName', 'style'=> 'border: 1px solid; text-transform: capitalize;'])}}
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <div class="row" style='margin: 5px 0px'>
                    @if($marchants->count() != 0)
                    @foreach($marchants as $marchant)
                    <div class="col-sm-4 padding-0 ">
                        <a href="{{ url('/shop/'.$marchant->slug) }}">
                            <div class="shop_card">
                                <!-- <img src="{{ url('web/images/category/161736464019c700x420.jpg') }}" height="275px"> -->
                                <img src="{{ url('public/imgs/logo/SwarnManthanWhite1024.png') }}" height="275px" style="object-fit: contain;">
                                <!-- <img src="{{ url('imgs/Swarnmanthan.jpg') }}" height="275px"> -->
                                <div class="shop_card_content" style="background: #f3b94c; position: relative;z-index: 999">
                                    <div style="position: absolute;left: 0;right: 0;top:0;bottom:0; z-index: 50;"></div>
                                    <p class="card-category text-white" style="text-transform: capitalize;">{{ $marchant->name }}</p>
                                </div>
                                <div class="shop_card_hover" style="background: #f3b94ca1;">
                                    <div class="text-center pt-5">
                                        <p style="font-size: 18px; color: #000; text-transform: capitalize;">
                                            <i class="fa fa-map-marker" style="font-size: 50px; color: #ff6936;"></i>
                                            <br>
                                            {{ $marchant->address }}<br>{{ $marchant->city->name }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @else
                    <div class="text-center">
                        <span>
                            No Marchants Found.
                        </span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('custom-scripts')
<script>
    $(document).ready(function() {
        $(".cat-nav li").click(function(e) {
            e.preventDefault();
            $(this).addClass("active"), $(this).parent().children("li").not(this).removeClass("active")
        });
        var e = "all",
            t = "all",
            a = "all",
            l = 1;

        function n(l) {
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "GET",
                url: "{{ url('homefilter') }}?page=" + l,
                data: {
                    filter: t,
                    s_filter: e,
                    c_filter: a,
                    main: "0"
                },
                datatype: "html",
                beforeSend: function() {
                    $(".load-more-btn").hide(), $(".ajax-loading").show()
                },
                success: function(e) {
                    console.log(e)
                },
                error: function(e, t, a) {
                    console.log(e)
                }
            }).done(function(e) {
                if (!e) return console.log("no"), $(".ajax-loading").hide(), 1 == l && $(".results").html("No Results Found!"), 0;
                $(".ajax-loading").hide(), 1 == l ? $(".results").html(e) : $(".results").append(e), $(e).find(".deal-block").length > 35 && $(".load-more-btn").show()
            }).fail(function(e, t, a) {
                alert("We are facing some issues currenlty. Please try again later.")
            })
        }
        $(".home-filter li").on("click change keyup", function() {
            t = $(".cat-nav li.active").attr("id"), e = $("#store-list").val(), a = $("#cat-list").val(), console.log(a), console.log(t), n(l = 1)
        }), $(".load-more-btn").on("click", function() {
            n(++l)
        })
    });
</script>
@endsection