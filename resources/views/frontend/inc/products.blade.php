@extends('frontend.layout.master')
@section('title','Products')
@section('contant')
<div class="forum-page-header mb-5" style="background: url('{{ url('public/imgs/headerimage/'.$setting->header_image) }}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">{{   ucwords($shop->name)}}</h2>
        </div>
    </div>
</div>
<section class="section">
    <div class="container">
        <div class="blog-page-main-block">
            <div class="blog-post-main">
                <!-- <h4 class="text-center mb-3 mt-5">All Category</h4> -->
                <div class="row" style='margin: 5px 0px'>
                @if(!$list->isEmpty())
                    @foreach($list as $category)
                    <div class="col-sm-4 padding-0 ">
                        <div class="cat_card">
                            <!-- <img src="{{ url('web/images/category/161736464019c700x420.jpg') }}" height="275px"> -->
                            <img src="{{ url('public/imgs/product/'.$category->image) }}" height="275px" style="object-fit: cover;">
                            <!-- <img src="{{ url('imgs/Swarnmanthan.jpg') }}" height="275px"> -->
                            <div class="cat_card_content" style="background: #f3b94c; position: relative;z-index: 999">
                                <div style="position: absolute;left: 0;right: 0;top:0;bottom:0; z-index: 50;"></div>
                                <p class="card-category text-white" style="text-transform: capitalize;">{{ $category->name }}</p>
                            </div>
                            <div class="cat_card_hover" style="background: #f3b94ca1;">
                                <div class="text-center pt-5">
                                    <p style="font-size: 18px; color: #000;">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div style="font-size:20px">Product Not Found</div>
                    @endif

                    
                </div>
            </div>
        </div>
    </div>
</section>

@endsection