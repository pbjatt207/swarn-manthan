@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('images/favicon/606c12fdf38eb.png'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">Terms And Conditions</h2>
        </div>
    </div>
</div>
<section id="about" class="coupon-page-main-block">

    <div class="container about-us-page">
        <div class="coupon-dtl-outer">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-main-block page-block">
                        <div class="about-section">
                            <p><span style="color: rgb(84, 74, 161); font-size: 30px;">Terms &amp; Conditions</span><br></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; font-size: 13px; line-height: 18px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; font-weight: bold;"><br></span></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; font-size: 13px; line-height: 18px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; font-weight: bold;">Deal Pricing</span>
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; font-weight: bold;"></span></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; font-size: 13px; line-height: 18px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <font face="Verdana">We feature all our deals in INR (Indian Rupee) only</font>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; font-size: 13px; line-height: 18px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <font face="Verdana">We accept payment in INR (Indian Rupee) only</font>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; font-weight: bold;"><br></span></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="font-family: inherit; font-size: inherit; font-variant-ligatures: inherit; font-variant-caps: inherit; font-weight: bold;">Refunds</span><br></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">We want all our customers at Rama Souq to be happy, so kindly read our refund policy to avoid any kind of disagreement:</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">1) We can't offer any kind of refund, store credit or replacement for any used or redeemed offer.<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">2) For unused vouchers we can provide you with a store credit if informed before the voucher expiry date in a written email (P.S. the email should be acknowledged by Rama Souq as well). Does not apply for issued E tickets, once E tickets are issued they cannot be refunded or cancelled in any case.<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">3) Expired vouchers can't be refunded or replaced<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">4) Refunds or Store Credits can't be processed in case of non availability of bookings on consumer's preferred dates and otherwise available before the voucher expiry date<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">5) Courier charges are not refundable<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">6) Refund will be considered if the merchant is at fault or unable to provide the service offered on Rama Souq. Also, the dispute has to be informed &amp; acknowledged by Rama Souq within the voucher validity period otherwise refund or store credit cannot be issued.</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">7) Buying any steal (offer) on Rama Souq is the sole responsibility of the customer and Rama Souq is not responsible for quality of the product or services provided by the listed merchant</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">8) For all other cases Rama Souq holds the rights to not issue any kind of refund or store credit<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <o:p style="font-family: Verdana;">&nbsp;</o:p>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline; font-weight: bold;">Payment Transaction</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Rama Souq accepts the following modes of payment:</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">1) Credit or Debit Card (Visa &amp; Master Card). There could be some exchange rate charges on non Indian cards, depending on your bank. Few debit cards are not accepted.<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">2) COD (Cash on Delivery of vouchers by our courier company to your door step).&nbsp;<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">3) You can also collect the vouchers from our office by paying us cash during our working hours. Kindly inform us at least a day before your visit. We work from Monday- Saturday from 9am - 5pm.<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">4) You can even deposit cash into our Bank account for the voucher amount. Email us the deposit slip at info@ramasouq.com and we will email you the vouchers after confirming with our bank.<o:p></o:p></span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <o:p style="font-family: Verdana;">&nbsp;</o:p>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline; font-weight: bold;">Payment Receipt &amp; Vouchers</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Your payment receipt is your voucher which can be downloaded from your Rama Souq' account profile by clicking on "download vouchers" section. Please note that we don't send vouchers to your email and you can download all your valid vouchers from your Rama Souq`s account profile. Vouchers will only be valid for particular the option that you have bought. Option will be mentioned on your voucher with its particular price. If you face any issues, please email us at info@ramasouq.com or call us&nbsp; (Monday- Saturday, 9am - 5pm)</span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <o:p style="font-family: Verdana;">&nbsp;</o:p>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; font-weight: bold;">Images &amp; Graphics</span>
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);"><span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Images used on the website are for illustrative purposes only unless mentioned otherwise on the steal.</span></p>
                        </div>
                    </div>
                </div>
                <!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					<div class="page-sidebar-widget popular-store-widget">
		<h6 class="widget-heading">Featured Stores</h6>
		<div id="sidebar-store-slider" class="sidebar-store-slider owl-carousel text-center">
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
					</div>
	</div>
	<div class="page-sidebar-widget recent-posts-widget">
		<h6 class="widget-heading">Recent Deals</h6>
		<ul>                                        
							<li><a href="https://ramasouq.com/post/awHws/spend-rs-400-toward-anything-on-menu-food-and-drinks" title="Post">Spend Rs. 400 Toward anything on Menu (Food and Drinks)</a></li>
							<li><a href="https://ramasouq.com/post/Cercr/home-schooling-at-magiqmind-for-pgnursery-classes" title="Post">Home schooling @ Magiqmind for PG/Nursery Classes</a></li>
							<li><a href="https://ramasouq.com/post/3TqsI/beauty-at-your-door-step-350" title="Post">Beauty at your Door Step - 350</a></li>
							<li><a href="https://ramasouq.com/post/4atKg/beauty-at-your-door-step-550" title="Post">Beauty at your Door Step - 550</a></li>
							<li><a href="https://ramasouq.com/post/oMfCe/full-body-oil-massage-60-mins" title="Post">Full Body Oil Massage - (60 Mins)</a></li>
					</ul>
	</div>
<div class="page-sidebar-widget sidebar-info-widget">
	<h6 class="widget-heading">RamaSouq</h6>
	<p>Launched in April 2021 in Jodhpur, Rama Souq is the Jodhpur’s biggest &#039;daily deals&#039;, or group buying, site featuring daily deals on the best things to do, see, eat and buy in the Jodhpur
We have office in Jodhpur</p>
</div>
							</div>
						</div>
						-->
            </div>
        </div>
    </div>
    </div>
</section>
@endsection