@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('{{ url('public/imgs/headerimage/'.$setting->header_image) }}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">Business Enquery</h2>
        </div>
    </div>
</div>
<section class="contact-section" style="padding: 0px 0 30px 0;">
    <div class="container">
        @if (\Session::has('success'))
        <div class="alert alert-success toast-msg" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
        @endif

        @if (\Session::has('danger'))
        <div class="alert alert-danger toast-msg" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
        @endif
        <div class="card p-5" style="height: auto;">
            <div class="contact-info margin-btm-mobile">
                {!! Form::open(['method' => 'POST', 'route'=>array('businesspost'), 'class' => 'contact-form']) !!}
                @csrf
                <div class="row">
                    <div class="col-md-6 form-group">
                        {{Form::label('name', 'Enter name')}}
                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Your name','required'=>'required'])}}
                    </div>
                    <div class="col-md-6 form-group">
                        {{Form::label('email', 'Enter email')}}
                        {{Form::text('email', '', ['class' => 'form-control', 'placeholder'=>'Your email','required'=>'required'])}}
                    </div>
                    <div class="col-md-6 form-group">
                        {{Form::label('mobile', 'Enter mobile')}}
                        {{Form::text('mobile', '', ['class' => 'form-control', 'placeholder'=>'Your mobile','required'=>'required'])}}
                    </div>
                    <div class="col-md-6 form-group">
                        {{Form::label('Shop_name', 'Enter shop name')}}
                        {{Form::text('shop_name', '', ['class' => 'form-control', 'placeholder'=>'Your shop name','required'=>'required'])}}
                    </div>
                    <div class="col-md-6 form-group">
                        {{Form::label('city', 'Select city')}}
                        {{Form::select('city', $cityArr,'', ['class' => 'form-control','required'=>'required','style'=>'height: auto !important;'])}}
                    </div>
                    <div class="col-md-6 form-group">
                        {{Form::label('address', 'Enter address')}}
                        {{Form::textarea('address', '', ['class' => 'form-control', 'placeholder'=>'Your address','style'=>'padding:10px; height:100px;','required'=>'required'])}}
                    </div>
                    <div class="col-md-9"></div>
                    <div class="col-md-3 form-group">
                        <button type="submit" class="site-btn float-right">SUBMIT</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    </div>
</section>

@endsection