@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('{{ url('public/imgs/headerimage/'.$setting->header_image) }}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">About Us</h2>
        </div>
    </div>
</div>
<section id="about" class="coupon-page-main-block">

    <div class="container about-us-page">
        <div class="coupon-dtl-outer">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-main-block page-block">
                        <div class="about-section">
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="font-family: Verdana; font-size: inherit; font-variant-ligatures: inherit; font-variant-caps: inherit;">We
                                    are here for the weekend loving (but not limited to weekends only) Jodhpur
                                    people by giving them good offers on their favorite services, products and
                                    getaway destinations in and nearby Jodhpur. Mind you, we are not offering you
                                    regular deals rather we are offering you mind blowing STEALS in Jodhpur. STEALS,
                                    yes, thats what we call them because we offer more than deals to our
                                    consumers.<br></span><br>We sell and advertise the various local discounted deal
                                (i.e. Spa, Beauty, Food &amp; Dining, Auto, Activities Leisure &amp; Event Pass,
                                Kids &amp; Games, Health &amp; Fitness, Services, Course &amp; Trainings, Retail
                                etc.)
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <br>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                We only sell stuff we ourselves would buy</p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                It is not about being cheap, It is about presenting you with the Best merchants
                                promoting themselves in a truly latest trends. Some call it value for money, some
                                call it a discount. and some call it a bargain. For us - it is a whole new way of
                                experiencing your city and your lifestyle. These are deals we buy for ourselves. If
                                they are not good enough for the folks at Rama Souq, then it does not go up on the
                                sale.&nbsp;</p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <br>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">So
                                    join our mantra for better online shopping experience and Lets Steal!</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><br></span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">We
                                    love feedback</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">We
                                    are not always right and are more than happy to admit that we are eager to
                                    learn, happy to response and believer in constant improvement.</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><br></span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Contact
                                    us</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="font-family: Verdana; font-size: inherit; font-variant-ligatures: inherit; font-variant-caps: inherit;">You
                                    may contact us at the details mentioned below:</span><br>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;"><br></span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Name
                                    :&nbsp; Rama Souq - Lifestyle &amp; Happiness</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Address
                                    : Plot no. 26, Mahaveerpuram City, Jodhpur, Rajasthan - 342008</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Email
                                    : info@ramasouq.com</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">Contact
                                    no. :&nbsp; 91166 73793</span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <span style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: Verdana; vertical-align: baseline;">
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <br>
                            </p>
                        </div>
                    </div>
                </div>
                <!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					<div class="page-sidebar-widget popular-store-widget">
		<h6 class="widget-heading">Featured Stores</h6>
		<div id="sidebar-store-slider" class="sidebar-store-slider owl-carousel text-center">
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
					</div>
	</div>
	<div class="page-sidebar-widget recent-posts-widget">
		<h6 class="widget-heading">Recent Deals</h6>
		<ul>                                        
							<li><a href="https://ramasouq.com/post/awHws/spend-rs-400-toward-anything-on-menu-food-and-drinks" title="Post">Spend Rs. 400 Toward anything on Menu (Food and Drinks)</a></li>
							<li><a href="https://ramasouq.com/post/Cercr/home-schooling-at-magiqmind-for-pgnursery-classes" title="Post">Home schooling @ Magiqmind for PG/Nursery Classes</a></li>
							<li><a href="https://ramasouq.com/post/3TqsI/beauty-at-your-door-step-350" title="Post">Beauty at your Door Step - 350</a></li>
							<li><a href="https://ramasouq.com/post/4atKg/beauty-at-your-door-step-550" title="Post">Beauty at your Door Step - 550</a></li>
							<li><a href="https://ramasouq.com/post/oMfCe/full-body-oil-massage-60-mins" title="Post">Full Body Oil Massage - (60 Mins)</a></li>
					</ul>
	</div>
<div class="page-sidebar-widget sidebar-info-widget">
	<h6 class="widget-heading">RamaSouq</h6>
	<p>Launched in April 2021 in Jodhpur, Rama Souq is the Jodhpur’s biggest &#039;daily deals&#039;, or group buying, site featuring daily deals on the best things to do, see, eat and buy in the Jodhpur
We have office in Jodhpur</p>
</div>
							</div>
						</div>
						-->
            </div>
        </div>
    </div>
    </div>
</section>

@endsection