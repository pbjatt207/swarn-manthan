@extends('frontend.layout.master')
@section('title',$page->title)
@section('contant')

<div class="forum-page-header mb-5" style="background: url('{{ url('public/imgs/headerimage/'.$setting->header_image) }}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">{{ $page->title }}</h2>
        </div>
    </div>
</div>
<section id="about" class="coupon-page-main-block">
    <div class="container about-us-page">
        <div class="coupon-dtl-outer">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-main-block page-block">
                        <div class="about-section">
                            {!! $page->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

@endsection