@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('public/images/favicon/606c12fdf38eb.png'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">How it works</h2>
        </div>
    </div>
</div>
<section id="about" class="coupon-page-main-block">

    <div class="container about-us-page">
        <div class="coupon-dtl-outer">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-main-block page-block">
                        <div class="about-section">
                            <h1 style="margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 30px; line-height: 36px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(84, 74, 161); text-rendering: optimizelegibility;">How it works</h1>
                            <div class="banner" style="margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 13px; line-height: inherit; vertical-align: baseline; color: rgb(51, 51, 51);">
                                <div class="get_it" style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
                                    <h2 style="margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 24px; line-height: 36px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(84, 74, 161); text-rendering: optimizelegibility;">Get It</h2>
                                    <p style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline;">Check your email, Facebook or Instagram feeds for daily steals on cool local businesses.</p>
                                </div>
                                <div class="get_it" style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
                                    <h3 style="margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 18px; line-height: 27px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(84, 74, 161); text-rendering: optimizelegibility;">Buy It</h3>
                                    <p style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline;">Add steals to your shopping cart and pay by credit card or online banking.</p>
                                </div>
                                <div class="get_it" style="margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
                                    <h4 style="margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 14px; line-height: 18px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(84, 74, 161); text-rendering: optimizelegibility;">Enjoy It</h4>
                                    <p style="margin-bottom: 9px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 13px; line-height: 18px; vertical-align: baseline;">Print the voucher or bring it up on your mobile device, then present it at the business to get your deal.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					<div class="page-sidebar-widget popular-store-widget">
		<h6 class="widget-heading">Featured Stores</h6>
		<div id="sidebar-store-slider" class="sidebar-store-slider owl-carousel text-center">
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
					</div>
	</div>
	<div class="page-sidebar-widget recent-posts-widget">
		<h6 class="widget-heading">Recent Deals</h6>
		<ul>                                        
							<li><a href="https://ramasouq.com/post/awHws/spend-rs-400-toward-anything-on-menu-food-and-drinks" title="Post">Spend Rs. 400 Toward anything on Menu (Food and Drinks)</a></li>
							<li><a href="https://ramasouq.com/post/Cercr/home-schooling-at-magiqmind-for-pgnursery-classes" title="Post">Home schooling @ Magiqmind for PG/Nursery Classes</a></li>
							<li><a href="https://ramasouq.com/post/3TqsI/beauty-at-your-door-step-350" title="Post">Beauty at your Door Step - 350</a></li>
							<li><a href="https://ramasouq.com/post/4atKg/beauty-at-your-door-step-550" title="Post">Beauty at your Door Step - 550</a></li>
							<li><a href="https://ramasouq.com/post/oMfCe/full-body-oil-massage-60-mins" title="Post">Full Body Oil Massage - (60 Mins)</a></li>
					</ul>
	</div>
<div class="page-sidebar-widget sidebar-info-widget">
	<h6 class="widget-heading">RamaSouq</h6>
	<p>Launched in April 2021 in Jodhpur, Rama Souq is the Jodhpur’s biggest &#039;daily deals&#039;, or group buying, site featuring daily deals on the best things to do, see, eat and buy in the Jodhpur
We have office in Jodhpur</p>
</div>
							</div>
						</div>
						-->
            </div>
        </div>
    </div>
    </div>
</section>
@endsection