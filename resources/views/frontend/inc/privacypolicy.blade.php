@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('public/images/favicon/606c12fdf38eb.png'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">Privacy Policy</h2>
        </div>
    </div>
</div>
<section id="about" class="coupon-page-main-block">

    <div class="container about-us-page">
        <div class="coupon-dtl-outer">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-main-block page-block">
                        <div class="about-section">
                            <h1 align="center" style="text-align:center"><u><span lang="X-NONE"><span style="font-family: Arial;">Privacy Policy</span>
                                        <o:p></o:p>
                                    </span></u></h1>
                            <p class="MsoNormal" style="text-align:justify">
                                <o:p><span style="font-family: Arial;"> </span></o:p>
                            </p>
                            <ol style="margin-top:0in" start="1" type="1"><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">The
                                            key information is collected mandatorily during the booking process such
                                            as customer’s name, address, mobile, phone numbers and email address etc.
                                            The personal information that we collect are used for communication to
                                            continuously provide and improve our services. This will not be used or
                                            shared with anyone else. Unless you ask not to, we may contact you via
                                            email or SMS in the future to tell you about special offers, new services
                                            or changes on this private policy.</span>
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">Personal
                                            data refers to data, whether true or not, about an individual: (i) who can
                                            be identified from that data; or (ii) from that data and other information
                                            to which we have or are likely to have access. All information you provide
                                            to us is stored on our secure servers. All credit/debit card details and
                                            personally identifiable information will NOT be stored, sold, shared,
                                            rented or leased to any third parties. Any payment transactions will be
                                            encrypted using SSL technology. Where we have given you (or where you have
                                            chosen) a password which enables you to access certain parts of the Ramasouq
                                            Platform, you are responsible for keeping this password confidential. We
                                            ask you not to share a password with anyone.</span>
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                            </ol>
                            <p class="MsoNormal" style="margin-left:.5in;text-align:justify"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">Unfortunately,
                                        the transmission of information via the internet is not completely secure.
                                        Although we will do our best to protect your personal data, we cannot guarantee
                                        the security of your data transmitted to Ramasouq Platform; any transmission is
                                        at your own risk. Once we have received your information, we will use strict
                                        procedures and security features to try to prevent unauthorized access.</span>
                                    <o:p></o:p>
                                </span></p>
                            <ol style="margin-top:0in" start="3" type="1"><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">If you
                                            make a payment for our services on our website, the details you are asked
                                            to submit will be provided directly to our payment provider via a secured
                                            connection.</span>
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-family: Arial;">Information
                                        provided to third party website linked to Ramasouq will not be our
                                        responsibility. The said third party websites will have their own privacy
                                        policy that will apply and Ramasouq will not be responsible for such and
                                        any questions regarding the said rules shall be directly discussed to the
                                        same third party entities.</span><span style="font-size:12.0pt;mso-bidi-font-size:
     11.0pt;line-height:115%">
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-family: Arial;">As
                                        website policies and Terms & Conditions are subject to occasional
                                        changes in our efforts to meet the security requirements and standards. Ramasouq
                                        encourages our customers to regularly visit these sections to be updated
                                        of any current and future changes. Any such modifications will be
                                        effective on the day of posting.</span><span style="font-size:12.0pt;mso-bidi-font-size:
     11.0pt;line-height:115%">
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-family: Arial;">Information
                                        collected by Ramasouq from our customers will be used for the following:</span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%">
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                            </ol>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l1 level1 lfo3">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To publish in social media or on Ramasouq Platform for the
                                        promotion of the company or the Ramasouq Platform</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:.75in;text-align:justify"><span style="font-family: Arial;">Social media
                                    publication or on Ramasouq platform for the promotion of the company or the
                                    platform</span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:
115%">
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l1 level1 lfo3">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To make suggestions and recommendations to you and other
                                        users of the Ramasouq Platform about services that may interest you or them
                                        information we receive from other sources. We may combine this information with
                                        information you give to us and information we collect about you. We may use
                                        this information and the combined information for the purposes set out above
                                        (depending on the types of information we receive)</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:.75in;text-align:justify"><span style="font-family: Arial;">To make
                                    suggestions and recommendations to you and other Ramasouq users about services
                                    that may be of interest to you and to other Ramasouq users. We may combine
                                    information received from other sources, information you give to us and
                                    information we collect about you for the purposes set above depending on the
                                    types of the said information.</span><span style="font-size:12.0pt;mso-bidi-font-size:
11.0pt;line-height:115%">
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To administer the Ramasouq Platform and for internal
                                        operations, including troubleshooting, data analysis, testing, research,
                                        statistical and survey purposes</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:.5in;text-align:justify"><span style="font-family: Arial;">To administer
                                    the Ramasouq Platform and for internal operations, including but might not be
                                    limited to troubleshooting, data analysis, testing, research, statistical and
                                    survey purposes</span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%">
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To allow you to participate in interactive features of our
                                        services, if any, when you choose to do so</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:.5in;text-align:justify"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">To allow
                                        interactive features on the platform where Users can participate in case they
                                        choose to do so</span>
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">As part of our efforts to keep the Ramasouq Platform safe and
                                        secure</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="text-align:justify;text-indent:.5in"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">As an
                                        integral part of our efforts to maintain a secure and safe platform</span>
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To improve the Ramasouq Platform to ensure that content is
                                        presented in the most effective manner for you and for your computer or
                                        smartphone.</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:.5in"><span style="font-family: Arial;">To improve the Ramasouq Platform to
                                    ensure that content is presented in the most effective manner for you on your
                                    computer or smartphone.</span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%">
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To measure or understand the effectiveness of advertising we
                                        serve to you and others, and to deliver relevant advertising to you</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:.25in;text-align:justify;text-indent:
.25in"><span style="font-family: Arial;">To measure, understand and deliver effective advertising to you and
                                    other Ramasouq users</span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%">
                                    <o:p></o:p>
                                </span></p>
                            <ol style="margin-top:0in" start="7" type="1"><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">Information
                                            you give to us. We will use this information:</span>
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                            </ol>
                            <p class="MsoNormal" style="text-align:justify"><br><span style="font-family: Arial;">
                                    Information given by our customers through the platform will be used for the
                                    following:</span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:
115%">
                                    <o:p></o:p>
                                </span></p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To send you Service-related notices. We may also use your
                                        contact information to send you marketing email messages. If you do not want to
                                        receive such messages, you may opt out by following the instructions in the
                                        message. In order to verify that the email address provided by you belongs to
                                        you, we will send you a verification email. Once, the verification process is
                                        complete, your account will be fully functional.</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To share with Suppliers to improve services of suppliers</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To update and back-up our records</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To carry out our obligations arising from any contracts
                                        entered into between you and us </span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-indent:-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:
Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To communicate with you, including confirming and updating
                                        you on the status of the Services and responding to your queries and requests.</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To inform you of promotions, offers, surveys, events, products
                                        and services which may be of interest to you</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size: 12pt; line-height: 115%; font-family: Arial;">F</span><span style="font-family: Arial;">or Ramasouq and other selected third parties to
                                    provide you with information about service we feel may be of interest to you.
                                    Upon your consent, Ramasouq and other selected third parties will contact you only
                                    electronically via email or sms regarding the said services. Should you wish to
                                    opt out from your data being used as such or being shared to our selected third
                                    parties for marketing purposes, you can write to us at </span><a href="mailto:support@ilaj.com" target="_blank"><span style="color: windowtext; font-family: Arial;">info@Ramasouq.com</span></a><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%">
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-family: Arial;">To ensure that content is presented in the most effective manner
                                    for you on your computer or smartphone.</span><span style="font-size:12.0pt;
mso-bidi-font-size:11.0pt;line-height:115%">
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To notify you about changes to our services</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To comply with applicable Law, the requests of Law
                                        enforcement and regulatory officials, or orders of court.</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To process the verification of your personal key information
                                        particulars and payment</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To personalize content</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To be able to facilitate the Services offered through the Ramasouq
                                        platform in the most efficient manner possible </span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To provide you with information about other services we offer
                                        that are similar to those that you are already using or have enquired about</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To enhance the security of the Ramasouq Platform</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To minimize credit risk, avoid errors, eliminate fraud and
                                        prevent any other criminal activity</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To remember information to help you efficiently access your
                                        account</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p class="MsoNormal" style="margin-left:1.0in;text-align:justify;text-indent:
-.25in;mso-list:l0 level1 lfo2">
                                <!--[if !supportLists]--><span style="font-size:
12.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:Wingdings;
mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings"><span style="font-family: Arial;">Ø</span><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: Arial;">  </span></span>
                                <!--[endif]--><span dir="LTR"></span><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
line-height:115%"><span style="font-family: Arial;">To enforce our legal rights and remedies</span>
                                    <o:p></o:p>
                                </span>
                            </p>
                            <p><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span><span style="font-family: Arial;">

                                </span></p>
                            <ol style="margin-top:0in" start="8" type="1"><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">The
                                            privacy policy is a part of and incorporated within and is to be read
                                            along with the Terms of Use (“Terms”). The capitalized terms used in this
                                            Privacy Policy, but not defined herein, shall have the meaning given to
                                            such terms in the Terms.</span>
                                        <o:p></o:p>
                                    </span></li><span style="font-family: Arial;">
                                </span>
                                <li class="MsoNormal" style="text-align:justify;mso-list:l2 level1 lfo1"><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:115%"><span style="font-family: Arial;">This
                                            Privacy Policy is a legally binding document between you and Ramasouq. The
                                            Term of this Privacy Policy will be effective upon your acceptance of the
                                            same (Directly or Indirectly in Electronic Form, by Clicking on the I
                                            Accept Tab or By other Means </span><span style="font-family: Arial;"> </span><span style="font-family: Arial;">or By
                                            use of website) and will govern the relationship between you and Ramasouq.</span>
                                        <o:p></o:p>
                                    </span></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					<div class="page-sidebar-widget popular-store-widget">
		<h6 class="widget-heading">Featured Stores</h6>
		<div id="sidebar-store-slider" class="sidebar-store-slider owl-carousel text-center">
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
					</div>
	</div>
	<div class="page-sidebar-widget recent-posts-widget">
		<h6 class="widget-heading">Recent Deals</h6>
		<ul>                                        
							<li><a href="https://ramasouq.com/post/awHws/spend-rs-400-toward-anything-on-menu-food-and-drinks" title="Post">Spend Rs. 400 Toward anything on Menu (Food and Drinks)</a></li>
							<li><a href="https://ramasouq.com/post/Cercr/home-schooling-at-magiqmind-for-pgnursery-classes" title="Post">Home schooling @ Magiqmind for PG/Nursery Classes</a></li>
							<li><a href="https://ramasouq.com/post/3TqsI/beauty-at-your-door-step-350" title="Post">Beauty at your Door Step - 350</a></li>
							<li><a href="https://ramasouq.com/post/4atKg/beauty-at-your-door-step-550" title="Post">Beauty at your Door Step - 550</a></li>
							<li><a href="https://ramasouq.com/post/oMfCe/full-body-oil-massage-60-mins" title="Post">Full Body Oil Massage - (60 Mins)</a></li>
					</ul>
	</div>
<div class="page-sidebar-widget sidebar-info-widget">
	<h6 class="widget-heading">RamaSouq</h6>
	<p>Launched in April 2021 in Jodhpur, Rama Souq is the Jodhpur’s biggest &#039;daily deals&#039;, or group buying, site featuring daily deals on the best things to do, see, eat and buy in the Jodhpur
We have office in Jodhpur</p>
</div>
							</div>
						</div>
						-->
            </div>
        </div>
    </div>
    </div>
</section>
@endsection