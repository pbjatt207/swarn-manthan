@php
$setting = App\Model\Setting::find(1);
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{ url('/') }}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<meta name="author" content="Shrawan Choudhary" /0
	<link rel="icon" type="image/icon" href="{{ url('imgs/favicon/'.$setting->favicon) }}">
	<meta name="_token" content="{{ csrf_token() }}">
	<title> @yield('title') | {{ $setting->title }}</title>

	<!-- theme styles -->
	<link href="{{ url('public/web/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- bootstrap css -->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/fontawesome/css/fontawesome-all.min.css') }}" />
	<!-- fontawesome css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/css/font-awesome.min.css') }}" />
	<!-- fontawesome css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/flaticon/flaticon.css') }}" /> <!-- flaticon css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/owl/css/owl.carousel.min.css') }}" />
	<!-- owl carousel css -->
	<link rel="stylesheet" type="text/css" href="{{ url('public/web/vendor/datatables/css/responsive.datatables.min.css') }}" />
	<!-- datatables responsive -->
	<link href="{{ url('public/web/css/jquery.rateyo.css') }}" rel="stylesheet" type="text/css" />
	<!-- rateyo css -->
	<link href="{{ url('public/web/vendor/datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
	<!-- datepicker css -->
	<link rel="stylesheet" type="text/css" href="../cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css" /> <!-- summernote css -->
	<link href="{{ url('public/web/css/summernote-bs4.css') }}" rel="stylesheet" type="text/css" />
	<!-- summernote css -->
	<link href="{{ url('public/web/css/select2.css') }}" rel="stylesheet" type="text/css" />
	<!-- select css -->
	<link href="{{ url('public/web/css/style.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>

    <div class="forum-page-header " style="background:#ff6936; background-position: center;background-size: cover; background-repeat: no-repeat;">
        <div class="container">
            <div class="row m-0">
                <div class="col-3 col-lg-3 col-md-3 pr-0">
                    <div class="logo" style="line-height:98px">
                        <a href="{{ route('home') }}" title="Home">
                            <img src="{{ url('public/imgs/logo/'.$setting->logo) }}" class="img-fluid" alt="Logo">
                        </a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="forum-page-heading-block">
                        <h2 class="forum-page-heading text-center text-white">{{ $setting->title }}</h2>
                    </div>
                </div>
                <div class="col-3 col-lg-3 col-md-3 pr-0"></div>
            </div>
            
        </div>
    </div>
    <div class="forum-page-heading-block">
        <h2 class="forum-page-heading text-center">Add Marchant</h2>
    </div>
    <div class="container">
            
        @if (\Session::has('success'))
            <div class="alert alert-success toast-msg" style="color: green;">
                {!! \Session::get('success') !!}
            </div>
        @endif
    
        @if (\Session::has('danger'))
            <div class="alert alert-danger toast-msg" style="color: green;">
                {!! \Session::get('danger') !!}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger toast-msg text-white">
                @foreach ($errors->all() as $error)
                    <div class="">{{$error}}</div>
                @endforeach
            </div>
        @endif
    </div>
    
    <section class="contact-section pt-0">
        <div class="container">
        
            <div class="card-body" id="category_box">
            {!! Form::open(['method' => 'POST', 'route'=>array('add_marchant', $id), 'class' => 'contact-form mb-5']) !!}
                    
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                    {{Form::label('name', 'Enter name')}}
                    {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','id'=>'title','required'=>'required'])}}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                    {{Form::label('mobile', 'Enter mobile')}}
                    
                    {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="form-group">
                    {{Form::label('password', 'Enter password')}}
                    {{Form::text('record[password]', '', ['class' => 'form-control', 'placeholder'=>'Enter password','id'=>'title'])}}
                    </div>
                </div> -->
                <div class="col-lg-6">
                    <div class="form-group">
                    {{Form::label('email', 'Enter email')}}
                    {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Enter email','id'=>'title','required'=>'required'])}}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                    @php
                    $cities = App\Model\Plan::get();

                    @endphp
                    {{Form::label('plan_id', 'Select Plan')}}
                    <select class="form-control weight-select" id="plan_id" name="record2[plan_id]" required>
                        <option value="">Select Plan</option>
                        @foreach($cities as $ct)
                        <option value="{{ $ct->id }}">{{ $ct->name }}</option>
                        @endforeach
                    </select>
                    <!-- {{Form::select('record1[city_id]', $cityArr,'', ['class' => 'form-control','id'=>'title','required'=>'required'])}} -->
                    </div>
                </div>
                
            </div>
            <hr>
            <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                {{Form::label('name', 'Enter shop name')}}
                {{Form::text('record1[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter shop name','id'=>'title','required'=>'required'])}}
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                {{Form::label('address', 'Enter address')}}
                {{Form::textarea('record1[address]', '', ['class' => 'form-control', 'placeholder'=>'Enter address','id'=>'title','rows'=>'2', 'required'=>'required'])}}
                </div>
            </div>


            <div class="col-lg-6">
                <div class="form-group">
                @php
                $cities = App\Model\City::get();

                @endphp
                {{Form::label('city_id', 'Select City')}}
                <select class="form-control weight-select" id="city_id" name="record1[city_id]" required>
                    <option value="">Select City</option>
                    @foreach($cities as $ct)
                    <option data-pc="{{ $ct->pincode }}" value="{{ $ct->id }}">{{ $ct->name }}</option>
                    @endforeach
                </select>
                <!-- {{Form::select('record1[city_id]', $cityArr,'', ['class' => 'form-control','id'=>'title','required'=>'required'])}} -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                {{Form::label('pincode', 'Enter pincode')}}
                {{Form::text('record1[pincode]', '', ['class' => 'form-control', 'placeholder'=>'Enter pincode','id'=>'city_pc','required'=>'required'])}}
                </div>
            </div>
            </div>
                <div >
                    <input type="submit" class="site-btn btn float-right" style="background:#ff6936;" value="Submit"/>
                   
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </section>
    {{ HTML::script('public/assets/vendor/jquery/jquery.min.js') }}
	{{ HTML::script('public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}
	{{ HTML::script('public/assets/vendor/jquery-easing/jquery.easing.min.js') }}
	{{ HTML::script('public/assets/js/validation.js') }}
    
</body>
</html>