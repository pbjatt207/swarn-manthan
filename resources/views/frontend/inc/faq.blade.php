@extends('frontend.layout.master')
@section('title','Homepage')
@section('contant')

<div class="forum-page-header mb-5" style="background: url('public/images/favicon/606c12fdf38eb.png'); background-position: center;background-size: cover; background-repeat: no-repeat;">
    <div class="container">
        <div class="forum-page-heading-block">
            <h2 class="forum-page-heading text-center">FAQ</h2>
        </div>
    </div>
</div>
<section id="about" class="coupon-page-main-block">

    <div class="container about-us-page">
        <div class="coupon-dtl-outer">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-main-block page-block">
                        <div class="about-section">
                            <div class="panel-heading pos-rel" style="position: relative; color: rgb(88, 102, 113); font-family: Barlow;">
                                <h2 style="font-family: Barlow; line-height: 1.1; color: rgb(240, 123, 5); margin-top: 0px; margin-bottom: 0px; font-size: 1.4375rem; padding: 15px 0px;"><span style="font-family: Arial;">Frequently asked questions</span></h2>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px; font-family: Arial;"><span style="font-weight: 700;">What is Rama Souq?</span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="menu-list clearfix" style="font-size: 14px; color: rgb(88, 102, 113);"></div>
                                            <div class=" created-section" style="font-size: 14px; color: rgb(88, 102, 113);">
                                                <div class=" created-section"></div>
                                            </div>
                                            <div class="cms_area default-style" style="font-size: 14px; color: rgb(88, 102, 113);">
                                                <div class="panel-group faq_content" id="accordion">
                                                    <div class="panel panel-default" style="border-bottom-color: rgb(238, 238, 238); padding: 15px 0px;">
                                                        <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                        <div class="panel-collapse collapse in" id="content1" aria-expanded="true">
                                                            <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">Rama Souq helps you enjoy your local scene by offering amazing deals and incredible experiences at ridiculously low prices. For great local businesses, we deliver what you want most shiny, new customers who love to spread the word about you with their friends. And unlike similar offerings, we let you keep more of your hard-earned cash. What? You want more? Check out the page called About US</span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px; font-family: Arial;"><span style="font-weight: 700;">How much does it cost to join Rama Souq?</span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;"><span style="color: rgb(103, 103, 103); font-size: 15px; font-family: Arial;">Zip. Zero. Nada! It costs nothing to join.</span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;"><span style="color: rgb(103, 103, 103); font-size: 15px;"><br></span></span></font>
                                </h4>
                                <h2 style="font-family: Barlow; line-height: 1.1; color: rgb(240, 123, 5); margin-top: 0px; margin-bottom: 0px; font-size: 1.4375rem; padding: 30px 0px 20px;"><span style="font-family: Arial;">How our Deals work</span></h2>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content3" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">I love today's deal and I want to buy it.</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel panel-default" style="border-bottom-color: rgb(238, 238, 238); padding: 15px 0px; color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                <div class="panel-collapse collapse in" id="content3" aria-expanded="true">
                                                    <div class="panel-content" style="padding: 10px 0px 0px;">
                                                        <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">Simply click "buy" next to the deal and tell your friends to join in if you really, really want it. Each offer has a time limit (check out the clock) and if the minimum number of buyers required is reached, the offer 'goes live'.</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content6" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">Is my credit card info secure?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-heading pos-rel" style="position: relative; color: rgb(88, 102, 113); font-size: 14px;"></div>
                                            <div class="panel-collapse collapse in" id="content6" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">Absolutely! Your personal credit card information is transferred by SSL directly to a secure electronic vault. All transactions are encrypted for increased security. At no time is your credit card information stored on our servers.</span></p>
                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content7" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">How safe is my personal information?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                    <div class="panel-collapse collapse in" id="content7" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">We value your personal information and don't share it with anyone. For more information read our Privacy Policy.</span></p>
                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content8" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">When do deals end?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                            <div class="panel-collapse collapse in" id="content8" aria-expanded="true">
                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">All deals must be bought before the countdown clock hits zero.</span></p>
                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content9" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">Do I need to use my voucher the same day I buy it or before the countdown clock runs out?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                    <div class="panel-heading pos-rel" style="position: relative;"><span style="color: rgb(103, 103, 103); font-size: 15px; font-family: Arial;">No way. We would never cause you to miss work like that. All deals expire in a much longer period of time (usually two months). Check the expiration date to be sure on the deal write up.</span><br></div>
                                                                    <div class="panel-collapse collapse in" id="content9" aria-expanded="true">
                                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content10" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">What happens once there is no more time to buy the deal?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                            <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                                            <div class="panel-collapse collapse in" id="content10" aria-expanded="true">
                                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">Sorry. You just missed out.</span></p>
                                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content11" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">I bought the deal, what happens next?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                                    <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                                                    <div class="panel-collapse collapse in" id="content11" aria-expanded="true">
                                                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">Once you're credit card is charged, you will receive an email notification with a link to sign in and print your voucher. It has the offer details and instructions on how to redeem your sweet deal! You can also print your voucher from your membership account. Just login, check your orders and hit print!</span></p>
                                                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px; font-family: Arial;"><span style="font-weight: 700;">Can I buy a Rama Souq voucher as a gift for someone else?</span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;"><span style="color: rgb(103, 103, 103); font-size: 15px; font-family: Arial;">You sure can you lovely person. Unless the terms and conditions of the deal state otherwise.</span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;"><span style="color: rgb(103, 103, 103); font-size: 15px;"><br></span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content15" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">I sent the deal to my email but I don't see it in my inbox. What next?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-heading pos-rel" style="position: relative; color: rgb(88, 102, 113); font-size: 14px;"></div>
                                            <div class="panel-collapse collapse in" id="content15" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">Relax boss, we won't leave you hanging, we'll work through this together. First, check your spam folder (especially if you have a Yahoo email address) and see if your deal email was sent there. If it's not there, send our great Customer Services team an email. We can make sure your deal isn't lost out in the wilderness for too long.</span></p>
                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;"><a class="" data-parent="#accordion" data-toggle="collapse" href="https://www.yallabanana.com/pages/FAQ/#content16" aria-expanded="true" style="color: rgb(61, 61, 61); font-weight: 600; outline: 0px; display: block; position: relative; padding-right: 30px;"><span style="font-family: Arial;">What if I have a question about today's deal?</span></a></h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                    <div class="panel-collapse collapse in" id="content16" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">We suggest you take any questions you have about a particular deal straight to that business. All contact information along with the general information about the deal is listed in the daily write-up. Beyond that, the business can probably answer your more specific questions. If not, you know you can always email our great Customer Services team here or call during working hours and someone will assist you.</span></p>
                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px; font-family: Arial;"><span style="font-weight: 700;">The business won't redeem my voucher and it has not expired.</span></span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="cursor: pointer; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <div class="panel-collapse collapse in" id="content17" aria-expanded="true">
                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><span style="font-family: Arial;">If you have trouble redeeming your voucher with the business, please contact us.</span></p>
                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><span style="font-weight: 700;"><br></span>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-collapse collapse in" aria-expanded="true" style="color: rgb(88, 102, 113); font-size: 14px;">
                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                            <div class="panel-collapse collapse in" aria-expanded="true">
                                                                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                                    <div class="panel-collapse collapse in" aria-expanded="true">
                                                                                        <div class="panel-content" style="padding: 10px 0px 0px;">
                                                                                            <div class="panel-heading pos-rel" style="position: relative;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                            <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                    <p style="margin-bottom: 0px; font-size: 15px; padding: 0px; color: rgb(103, 103, 103);"><br></p>
                                                </div>
                                            </div>
                                        </span></font>
                                </h4>
                                <h4 style="font-family: inherit; line-height: 1.1; margin-top: 0px; margin-bottom: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d"><span style="outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;">
                                            <div class="panel-heading pos-rel" style="font-size: 14px; position: relative; color: rgb(88, 102, 113);"></div>
                                        </span></font>
                                </h4>
                                <h4 style="box-sizing: border-box; font-family: inherit; font-weight: 300; line-height: 1.1; color: inherit; margin: 0px; font-size: 16px; padding: 0px; text-decoration-line: underline;">
                                    <font color="#3d3d3d" style="box-sizing: border-box;"><span style="box-sizing: border-box; outline-color: initial; outline-style: initial; display: block; position: relative; padding-right: 30px;"><span style="box-sizing: border-box; color: rgb(103, 103, 103); font-size: 15px;"><br style="box-sizing: border-box;"></span></span></font>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="content2" aria-expanded="false" style="color: rgb(88, 102, 113); font-family: Barlow; height: 0px;">
                                <div class="panel-content" style="padding: 10px 0px 0px;">
                                    <p style="margin-bottom: 0px; padding: 0px; font-size: 15px; color: rgb(103, 103, 103);">Zip. Zero. Nada! It costs nothing to join.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					<div class="page-sidebar-widget popular-store-widget">
		<h6 class="widget-heading">Featured Stores</h6>
		<div id="sidebar-store-slider" class="sidebar-store-slider owl-carousel text-center">
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
							<div class="brand-img-two">
					<a href="https://ramasouq.com/store-dtl" title=""><img src="https://ramasouq.com/images/store" class="img-fluid" alt="Store"></a>
				</div>
					</div>
	</div>
	<div class="page-sidebar-widget recent-posts-widget">
		<h6 class="widget-heading">Recent Deals</h6>
		<ul>                                        
							<li><a href="https://ramasouq.com/post/awHws/spend-rs-400-toward-anything-on-menu-food-and-drinks" title="Post">Spend Rs. 400 Toward anything on Menu (Food and Drinks)</a></li>
							<li><a href="https://ramasouq.com/post/Cercr/home-schooling-at-magiqmind-for-pgnursery-classes" title="Post">Home schooling @ Magiqmind for PG/Nursery Classes</a></li>
							<li><a href="https://ramasouq.com/post/3TqsI/beauty-at-your-door-step-350" title="Post">Beauty at your Door Step - 350</a></li>
							<li><a href="https://ramasouq.com/post/4atKg/beauty-at-your-door-step-550" title="Post">Beauty at your Door Step - 550</a></li>
							<li><a href="https://ramasouq.com/post/oMfCe/full-body-oil-massage-60-mins" title="Post">Full Body Oil Massage - (60 Mins)</a></li>
					</ul>
	</div>
<div class="page-sidebar-widget sidebar-info-widget">
	<h6 class="widget-heading">RamaSouq</h6>
	<p>Launched in April 2021 in Jodhpur, Rama Souq is the Jodhpur’s biggest &#039;daily deals&#039;, or group buying, site featuring daily deals on the best things to do, see, eat and buy in the Jodhpur
We have office in Jodhpur</p>
</div>
							</div>
						</div>
						-->
            </div>
        </div>
    </div>
    </div>
</section>
@endsection