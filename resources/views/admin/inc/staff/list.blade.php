<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          {{ Form::open(['url' => url(env('ADMIN_DIR').'/staff')]) }}
          <!-- Page Heading -->
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="card">
                <div class="card-header d-sm-flex align-items-center justify-content-between mb-4">
                  <h6 class="m-0 font-weight-bold text-primary">Staff List</h6>
                  <div class="">
                    <a href="{{ route('staff.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">+ Add Staff</a>
                    <!-- <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-url="{{ url(env('ADMIN_DIR').'/staff/delete') }}" id="delete_all">Delete</button> -->

                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                      <tr>
                        <th>S No.</th>
                        <th >Name</th>
                        <!-- <th>Shop Name</th> -->
                        <th>mobile</th>
                        <th>email</th>
                        <th>Aadhar No.</th>
                        <th>Aadhar Photo</th>
                        <th>City</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($lists as $key => $list)
                      <tr class="bg-light">
                        <td>{{ $key+1 }}.
                          <!-- <input type="checkbox" name="sub_chk[]" value="{{ $list->id }}" class="sub_chk" data-id="{{$list->id}}"> -->
                        </td>
                        <td>
                          <a href="{{route('staff.edit', $list->id) }}">
                            <i class="far fa-edit" aria-hidden="true"></i> {{$list->name}} 
                          </a>
                        </td>
                        <td>{{$list->mobile}}</td>
                        <td>{{$list->email}}</td>
                        <td>
                            {{ $list->aadhar_no }}
                        </td>
                        <td>
                          <a href="{{ url('public/imgs/staff/'.$list->aadhar_photo) }}" target="_blank">
                            <img src="{{ url('public/imgs/staff/'.$list->aadhar_photo) }}" alt="" width="50">
                          </a>
                        </td>
                        <td>{{ $list->city->name }}</td>
                        <!-- <td>
                           <a href="{{url(env('ADMIN_DIR').'/testimonial/delete', $list->id)}}"  class="btn btn-danger btn-sm"
                            data-tr="tr_{{$list->id}}"
                            data-toggle="confirmation"
                            data-btn-ok-label="Delete" data-btn-ok-icon="fa fa-remove"
                            data-btn-ok-class="btn btn-sm btn-danger"
                            data-btn-cancel-label="Cancel"
                            data-btn-cancel-icon="fa fa-chevron-circle-left"
                            data-btn-cancel-class="btn btn-sm btn-default"
                            data-title="Are you sure you want to delete ?"
                            data-placement="left" data-singleton="true"
                            onclick="return confirm('Are you sure you want to delete this item?');">
                             Delete</a>
                         </td> -->
                        <!-- <td>
                          @if($list->status == 'disable')
                          <a href="{{ route('marchantchangestatus', [$list->id, 'field' => 'status', 'status' => 'enable', 'id' => $list->id]) }}" class="btn btn-success text-whites">Enable</a>
                          @else
                          <a href="{{ route('marchantchangestatus', [$list->id, 'field' => 'status', 'status' => 'disable', 'id' => $list->id]) }}" class="btn btn-danger text-white">Disable</a>
                          @endif
                        </td> -->
                        <td>
                          <a href="{{ url('/marchant/create/'.$list->id) }}" target="_blank">Add Marchant Link</a>
                        </td>

                      </tr>

                      @endforeach
                    </tbody>
                  </table>

                </div>

              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>