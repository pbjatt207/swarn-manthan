@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('name', 'Enter name')}}
      {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Enter name','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('mobile', 'Enter mobile')}}
      @if($edit)
      {{Form::number('mobile', $edit->mobile, ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
      @else
      {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
      @endif
    </div>
  </div>
  <!-- <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('password', 'Enter password')}}
      {{Form::text('password]', '', ['class' => 'form-control', 'placeholder'=>'Enter password','id'=>'title'])}}
    </div>
  </div> -->
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('email', 'Enter email')}}
      @if($edit)
      {{Form::email('email', $edit->email, ['class' => 'form-control', 'placeholder'=>'Enter email','id'=>'title','required'=>'required'])}}
      @else
      {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Enter email','id'=>'title','required'=>'required'])}}
      @endif
    </div>
  </div>
<!-- <hr> -->
  <div class="col-lg-6">
    <div class="form-group">
      
      {{Form::label('city_id', 'Select City')}}
      
      {{Form::select('city_id', $cityArr,'0', ['class' => 'form-control', 'id'=>'city_id','required'=>'required'])}}
      <!-- {{Form::select('city_id]', $cityArr,'', ['class' => 'form-control','id'=>'title','required'=>'required'])}} -->
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('aadhar_no', 'Enter Aadhar no')}}
      @if($edit)
      {{Form::text('aadhar_no', $edit->aadhar_no, ['class' => 'form-control', 'placeholder'=>'Enter aadhar no','id'=>'title','required'=>'required'])}}
      @else
      {{Form::text('aadhar_no', '', ['class' => 'form-control', 'placeholder'=>'Enter aadhar no','id'=>'title','required'=>'required'])}}
      @endif
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('aadhar_photo', 'Select Aadhar Card Photo')}}
      {{Form::file('aadhar_photo', ['class' => 'form-control', 'placeholder'=>'Aadhar Card Photo','id'=>'aadhar_photo'])}}
      @if($edit)
      <img src="{{ url('public/imgs/staff/'.$edit->aadhar_photo) }}" alt="{{$edit->name}}" >
      @endif
    </div>
  </div>
  
</div>