<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          {{ Form::open(['url' => url(env('ADMIN_DIR').'/marchant')]) }}
          <!-- Page Heading -->
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="card">
                <div class="card-header d-sm-flex align-items-center justify-content-between mb-4">
                  <h6 class="m-0 font-weight-bold text-primary">Contact Enquery List</h6>
                  <div class="">
                    <!-- <a href="{{ route('marchant.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">+ Add Marchant</a> -->
                    <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-url="{{ route('contact-deleteall') }}" id="delete_all">Delete</button>

                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                      <tr>
                        <th>S No.</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($lists as $key => $list)
                      <tr class="bg-light">
                        <td>
                          <input type="checkbox" name="sub_chk[]" value="{{ $list->id }}" class="sub_chk" data-id="{{$list->id}}"> | {{ $key+1 }}.
                        </td>
                        <td>{{$list->name}}</td>
                        <td>{{$list->mobile}}</td>
                        <td>{{$list->email}}</td>
                        <td>{{$list->subject}}</td>
                        <td>{{$list->message}}</td>
                        <td>
                          @if($list->status == 'true')
                          <div class="btn btn-success text-whites">Confirmed</div>
                          @else
                          <a href="{{ route('contact-enquiry-status', [$list->id, 'field' => 'status', 'status' => 'true', 'id' => $list->id]) }}" class="btn btn-warning text-white">Pending</a>
                          @endif
                        </td>

                      </tr>

                      @endforeach
                    </tbody>
                  </table>

                </div>

              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>