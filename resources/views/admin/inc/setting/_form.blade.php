@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('title', 'Website Title')}}
      {{Form::text('title', '', ['class' => 'form-control', 'placeholder'=>'Enter Website Title','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('tagline', 'Website Tagline')}}
      {{Form::text('tagline', '', ['class' => 'form-control', 'placeholder'=>'Enter Website Tagline','id'=>'tagline','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('email', 'Email')}}
      {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Enter Email','id'=>'email','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('mobile', 'Mobile No.')}}
      {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Enter Mobile No.','id'=>'mobile','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      {{Form::label('logo', 'Select Logo')}}
      {{Form::file('logo', ['class' => 'form-control', 'placeholder'=>'Enter Logo'])}}
    </div>
  </div>
  <div class="col-lg-3">
    @if($edit->logo)
    <img src="{{ url('public/imgs/logo/'.$edit->logo) }}" alt="" title="{{ $edit->logo }}" width="100px">
    @endif
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      {{Form::label('favicon', 'Select Favicon')}}
      {{Form::file('favicon', ['class' => 'form-control', 'placeholder'=>'Enter Favicon'])}}
    </div>
  </div>
  <div class="col-lg-3">
    @if($edit->favicon)
    <img src="{{ url('public/imgs/favicon/'.$edit->favicon) }}" alt="" title="{{ $edit->favicon }}" width="100px">
    @endif
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      {{Form::label('video', 'Select Video')}}
      {{Form::file('video', ['class' => 'form-control', 'placeholder'=>'Enter Video'])}}
    </div>
  </div>
  <div class="col-lg-3 pt-4">
    @if($edit->video)
    <a href="{{ url('public/imgs/video/'.$edit->video) }}" target="_blank" title="{{ $edit->video }}">Video Link</a>
    <!-- <img src="{{ url('imgs/video/'.$edit->video) }}" alt="" title="{{ $edit->video }}" width="100px"> -->
    @endif
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      {{Form::label('header_image', 'Select Header Image')}}
      {{Form::file('header_image', ['class' => 'form-control', 'placeholder'=>'Enter Header Image'])}}
    </div>
  </div>
  <div class="col-lg-3 pt-4">
    @if($edit->header_image)
    <img src="{{ url('public/imgs/headerimage/'.$edit->header_image) }}" alt="" title="{{ $edit->header_image }}" width="100px">
    @endif
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('address', 'Address')}}
      {{Form::textarea('address', '', ['class' => 'form-control', 'placeholder'=>'Enter Address','id'=>'mobile','required'=>'required','style'=>'height: 100px'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('google_map', 'Map Link')}}
      {{Form::textarea('google_map', '', ['class' => 'form-control', 'placeholder'=>'Enter Map Link','id'=>'mobile','required'=>'required','style'=>'height: 100px'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('sms_api', 'SMS Api')}}
      {{Form::textarea('sms_api', '', ['class' => 'form-control', 'placeholder'=>'Enter SMS Api','id'=>'mobile','style'=>'height: 100px'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('product_show_limit', 'Default product show Limit')}}
      {{Form::number('product_show_limit', '', ['class' => 'form-control', 'placeholder'=>'Enter Product Show limit','id'=>'product_show_limit','required'=>'required'])}}
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <h5>Gold Detail :-</h5>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('gold_price', 'Gold Price')}}
      {{Form::number('gold_price', '', ['class' => 'form-control', 'placeholder'=>'Enter Gold Price','id'=>'gold_price','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('gold_weight', 'Gold Weight')}}
      {{Form::text('gold_weight', '', ['class' => 'form-control', 'placeholder'=>'Enter Gold Weight','id'=>'gold_weight','required'=>'required'])}}
    </div>
  </div>
  @php
  $goldArr = [
  '' => 'Select Unit',
  'gm' => 'Gm',
  'kg' => 'Kg'
  ];
  @endphp
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('gold_unit', 'Gold Unit')}}
      {{Form::select('gold_unit', $goldArr,'', ['class' => 'form-control','id'=>'gold_unit','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-12">
    <h5>Silver Detail :-</h5>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('silver_price', 'Silver Price')}}
      {{Form::number('silver_price', '', ['class' => 'form-control', 'placeholder'=>'Enter Silver Price','id'=>'silver_price','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('silver_weight', 'Silver Weight')}}
      {{Form::text('silver_weight', '', ['class' => 'form-control', 'placeholder'=>'Enter Silver Weight','id'=>'silver_weight','required'=>'required'])}}
    </div>
  </div>
  @php
  $silverArr = [
  '' => 'Select Unit',
  'gm' => 'Gm',
  'kg' => 'Kg'
  ];
  @endphp
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('silver_unit', 'Select Silver Unit')}}
      {{Form::select('silver_unit', $silverArr,'', ['class' => 'form-control','id'=>'silver_unit','required'=>'required'])}}
    </div>
  </div>

</div>
<hr>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('marchant_applink', 'Marchant App Link Link')}}
      {{Form::text('marchant_applink', '', ['class' => 'form-control', 'placeholder'=>'Enter Marchant App Link Link'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('user_applink', 'User App Link Link')}}
      {{Form::text('user_applink', '', ['class' => 'form-control', 'placeholder'=>'Enter User App Link Link'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('facebook', 'Facebook Link')}}
      {{Form::text('facebook', '', ['class' => 'form-control', 'placeholder'=>'Enter Facebook Link'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('instagram', 'Instagram Link')}}
      {{Form::text('instagram', '', ['class' => 'form-control', 'placeholder'=>'Enter Instagram Link'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('twitter', 'Twitter Link')}}
      {{Form::text('twitter', '', ['class' => 'form-control', 'placeholder'=>'Enter Twitter Link'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('youtube', 'Youtube Link')}}
      {{Form::text('youtube', '', ['class' => 'form-control', 'placeholder'=>'Enter Youtube Link'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('linkedin', 'Linkedin Link')}}
      {{Form::text('linkedin', '', ['class' => 'form-control', 'placeholder'=>'Enter linkedin Link'])}}
    </div>
  </div>
</div>