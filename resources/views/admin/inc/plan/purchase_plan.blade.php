<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
       @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Content Row -->
          @if (\Session::has('success'))
              <div class="alert alert-success toast-msg" style="color: green">
                  {!! \Session::get('success') !!}</li>
              </div>
          @endif

          @if (\Session::has('danger'))
              <div class="alert alert-danger toast-msg" style="color: red;">
                  {!! \Session::get('danger') !!}</li>
              </div>
          @endif
          <div class="row">
            
            <div class="col-xs-12 col-lg-12">
          {{ Form::open(['url' => url(env('ADMIN_DIR').'/Purchase_plan/')]) }}
              <div class="card">
                <div class="card-header d-sm-flex align-items-center justify-content-between mb-4">
                  <h6 class="m-0 font-weight-bold text-primary">Purchase Plan List</h6>
                  <!-- <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"  data-url="{{ url(env('ADMIN_DIR').'/Purchaseplan/delete') }}" id="delete_all">Delete</button> -->
                </div>
                <div class="card-body">
                 <table class="table table-bordered">
                   <thead class="thead-dark">
                     <tr>
                       <th>S No.</th>
                       <th>Marchant Name</th>
                       <th>Plan Name</th>
                       <th>Payment</th>
                       <th>Payment Status</th>
                       <th> Subscription Date </th>
                       <th> Expiry Date </th>
                       <th>Action</th>
                     </tr>
                   </thead>
                   <tbody>
                    @php
                    $sn = $lists->firstItem();
                    @endphp
                    @foreach($lists as $list)
                       <tr class="bg-light">
                        <td>{{ $sn++ }}.
                           <!-- | 
                            <input type="checkbox" name="sub_chk[]" value="{{ $list->id }}" class="sub_chk" data-id="{{$list->id}}"> -->
                        </td>
                         <td>{{$list->shop ? $list->shop->name : 'N/A'}}</td>
                         <td>{{$list->plan ? $list->plan->name : 'N/A'}}</td>
                         <td>₹{{ $list->payment ?? 'N/A' }}</td>
                         <td>{{ $list->payment_status ?? 'N/A' }}</td>
                         <td>{{$list->subscription_date ?? 'N/A'}}</td>
                         <td>{{ $list->expiry_date ?? 'N/A'}}</td>
                         <td>
                          @if($list->status  == "Active")
                          <a href="{{ route('purchagePlanchangestatus', [$list->id, 'field' => 'status', 'status' => 'Inactive', 'id' => $list->id]) }}" class="btn btn-success text-white">Active</a>
                          @else
                          <a href="{{ route('purchagePlanchangestatus', [$list->id, 'field' => 'status', 'status' => 'Active', 'id' => $list->id]) }}" class="btn btn-warning text-white">Inactive</a>
                          @endif
                        </td>
                        
                       </tr>
                     @endforeach
                   </tbody>
                 </table>

              {{  $lists->links() }}
                </div>

              </div>
          {{ Form::close() }}
            </div>
          </div>
         </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
