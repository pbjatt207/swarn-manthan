@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('name', 'Enter name')}}
      {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('mobile', 'Enter mobile')}}
      @if($edit)
      {{Form::number('mobile', $edit->mobile, ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
      @else
      {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
      @endif
    </div>
  </div>
  <!-- <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('password', 'Enter password')}}
      {{Form::text('record[password]', '', ['class' => 'form-control', 'placeholder'=>'Enter password','id'=>'title'])}}
    </div>
  </div> -->
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('email', 'Enter email')}}
      @if($edit)
      {{Form::email('email', $edit->email, ['class' => 'form-control', 'placeholder'=>'Enter email','id'=>'title','required'=>'required'])}}
      @else
      {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Enter email','id'=>'title','required'=>'required'])}}
      @endif
    </div>
  </div>
  @if($edit)
  <div class="col-lg-6">
    <div class="form-group">
     
      {{Form::label('plan_id', 'Select Plan')}}
      @if(!$purchase_plan)
      {{Form::select('record2[plan_id]', $planArr,'0', ['class' => 'form-control', 'id'=>'plan_id'])}}
      @else
      {{Form::select('record2[plan_id]', $planArr,'0', ['class' => 'form-control', 'id'=>'plan_id', 'disabled'=>'disabled'])}}
      @endif
    </div>
  </div>
  @endif
  @if(!$edit)
  <div class="col-lg-6">
    <div class="form-group">
     
      {{Form::label('plan_id', 'Select Plan')}}
      {{Form::select('record2[plan_id]', $planArr,'0', ['class' => 'form-control', 'id'=>'plan_id'])}}
    </div>
  </div>
  @endif
</div>

<hr>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('name', 'Enter shop name')}}
      {{Form::text('record1[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter shop name','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('address', 'Enter address')}}
      {{Form::textarea('record1[address]', '', ['class' => 'form-control', 'placeholder'=>'Enter address','id'=>'title','rows'=>'2', 'required'=>'required'])}}
    </div>
  </div>


  <div class="col-lg-6">
    <div class="form-group">
      @php
      $cities = App\Model\City::get();

      @endphp
      {{Form::label('city_id', 'Select City')}}
      {{Form::select('record1[city_id]', $cityArr,'0', ['class' => 'form-control', 'id'=>'city_id','required'=>'required'])}}
      <!-- <select class="form-control weight-select" id="city_id" name="record1[city_id]">
        <option value="">Select City</option>
        @foreach($cities as $ct)
        <option data-pc="{{ $ct->pincode }}" value="{{ $ct->id }}">{{ $ct->name }}</option>
        @endforeach
      </select> -->
      <!-- {{Form::select('record1[city_id]', $cityArr,'', ['class' => 'form-control','id'=>'title','required'=>'required'])}} -->
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('pincode', 'Enter pincode')}}
      {{Form::text('record1[pincode]', '', ['class' => 'form-control', 'placeholder'=>'Enter pincode','id'=>'city_pc','required'=>'required'])}}
    </div>
  </div>

  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('latitude', 'Enter latitude')}}
      {{Form::text('record1[latitude]', '', ['class' => 'form-control', 'placeholder'=>'Enter latitude','id'=>'latitude'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('longitude', 'Enter longitude')}}
      {{Form::text('record1[longitude]', '', ['class' => 'form-control', 'placeholder'=>'Enter longitude','id'=>'longitude'])}}
    </div>
  </div>
</div>