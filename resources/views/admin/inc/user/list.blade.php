<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          {{ Form::open(['url' => url(env('ADMIN_DIR').'/marchant'), 'method'=>'GET']) }}
          <!-- Page Heading -->
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="card">
                <div class="card-header d-sm-flex align-items-center justify-content-between mb-4">
                  <h6 class="m-0 font-weight-bold text-primary">Marchant List</h6>
                  <div class="">
                    <a href="{{ route('marchant.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">+ Add Marchant</a>
                    <!-- <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-url="{{ url(env('ADMIN_DIR').'/marchant/delete') }}" id="delete_all">Delete</button> -->

                  </div>
                </div>
                <div class="row card-body">
                  
                  <div class="col-lg-9 float-left" style="font-size: 18px;">{{ $lists->firstItem() }} - {{ $lists->lastItem() }} out of {{ $lists->total() }} record(s) showing.  </div>
                  <div class="col-lg-3 float-left" style="font-size: 18px;">
                    <div class="input-group">
                      {!! Form::text('search','',['class' => 'form-control', 'placeholder' => 'Search...'])!!}
                      <div class="input-group-append">
                        
                        {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  @if( !$lists->isEmpty() )
                  <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                      <tr>
                        <th>S No.</th>
                        <th colspan="2">Name</th>
                        <!-- <th>Shop Name</th> -->
                        <th>mobile</th>
                        <th>email</th>
                        <th>Address</th>
                        <th>Added By</th>
                        <th>Purchase Plan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($lists as $key => $list)
                      <tr class="bg-light">
                        <td>{{ $key+1 }}.
                          <!-- <input type="checkbox" name="sub_chk[]" value="{{ $list->id }}" class="sub_chk" data-id="{{$list->id}}"> -->
                        </td>
                        <td>
                          <img src="https://chart.googleapis.com/chart?chs=400x400&cht=qr&chl={{ url('shop/'.$list->shop_name->slug) }}&choe=UTF-8" alt="" style="width: 70px;">
                        </td>
                        <td>
                          <div>
                            <a href="{{route('marchant.edit', $list->id) }}">
                              <i class="far fa-edit" aria-hidden="true"></i> {{$list->name}} 
                            </a>
                          </div>
                          <div>
                            ({{$list->shop_name->name}})
                          </div>
                        </td>
                        <!-- <td>{{$list->shop_name->name}}</td> -->
                        <td>{{$list->mobile}}</td>
                        <td>{{$list->email}}</td>
                        <td>{{$list->shop_name->address}}, {{ $list->shop_name->city->name }}</td>
                        <td>{{$list->shop_name->staff ? $list->shop_name->staff->name.' (Staff)' : 'Admin'}}</td>
                        <td>{{$list->purchase_plan == 'null' ? 'N/A' : $list->purchase_plan->plan->name}}</td>
                        <!-- <td>
                           <a href="{{url(env('ADMIN_DIR').'/testimonial/delete', $list->id)}}"  class="btn btn-danger btn-sm"
                            data-tr="tr_{{$list->id}}"
                            data-toggle="confirmation"
                            data-btn-ok-label="Delete" data-btn-ok-icon="fa fa-remove"
                            data-btn-ok-class="btn btn-sm btn-danger"
                            data-btn-cancel-label="Cancel"
                            data-btn-cancel-icon="fa fa-chevron-circle-left"
                            data-btn-cancel-class="btn btn-sm btn-default"
                            data-title="Are you sure you want to delete ?"
                            data-placement="left" data-singleton="true"
                            onclick="return confirm('Are you sure you want to delete this item?');">
                             Delete</a>
                         </td> -->
                        <td>
                          @if($list->status == 'disable')
                          <a href="{{ route('marchantchangestatus', [$list->id, 'field' => 'status', 'status' => 'enable', 'id' => $list->id]) }}" class="btn btn-danger text-whites">Disable</a>
                          @else
                          <a href="{{ route('marchantchangestatus', [$list->id, 'field' => 'status', 'status' => 'disable', 'id' => $list->id]) }}" class="btn btn-success text-white">Enable</a>
                          @endif
                        </td>

                      </tr>

                      @endforeach
                    </tbody>
                  </table>

                  {{ $lists->links() }}
                  @else
                    No Record Found
                    @endif
                </div>

              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>