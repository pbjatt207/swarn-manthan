<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
       @include('admin.common.TopHeader')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Product</h6>
                  <a href="{{ url('admin/product') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> View Product</a>
                </div>

                 <!-- Card Body -->
                <div class="card-body">
                {!! Form::open(['method' => 'PUT', 'route'=>array('product.update', $edit['id']), 'class' => 'user', 'files'=>'true']) !!}
                      @include('admin.inc.product._form')
                      <div class="row">
                      <div class="col-lg-6">
                        <div  class="refresh_remove_image">
                      @if (!$productimage->isEmpty())
                        <h4>Product Images</h4>
                        <div>
                        
                        <table style="border:1px solid #d2d6de;width:100%">
                            <tr class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">Image</th>
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">status</th>
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">action</th>
                            </tr>
                            @foreach($productimage as $key=>$ci)
                            <tr class="text-center refresh">
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                                  <img src="{{ url('imgs/product/'.$ci->name) }}" width="70"> 
                                </td>
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;;">
                                
                                    <!-- <input data-id="{{$ci->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" {{ $ci->status ? 'checked' : '' }}> -->
                                    <select data-type="image" data-shop_id="{{$edit->shop_id}}" data-product_id="{{$edit->id}}" data-id="{{$ci->id}}" name="type" class="change_status">
                                      <option value="Approve" {{ $ci->type == 'Approve' ? 'selected' : '' }} >Approve</option>
                                      <option value="In-progress" {{ $ci->type == 'In-progress' ? 'selected' : '' }}>In-progress</option>
                                      <option value="Reject" {{ $ci->type == 'Reject' ? 'selected' : '' }}>Reject</option>
                                    </select>
                                    
                                </td>
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                                    <div class="btn-floating remove-image" data-type="image" data-id="{{ $ci->id }}"><i class="fas fa-trash-alt text-danger p-1"></i></div>
                                </td>
                                
                            </tr>
                            
                            @endforeach
                        </table>
                        
                          <!--<div class="row">-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--</div>-->
                        
                        </div>
                        @else 
                        @endif
                      
                        @if ($productvideo != null)
                        
                        <h4 class="mt-3">Product Video</h4>
                        <div>
                        
                        <table style="border:1px solid #d2d6de;width:100%">
                            <tr class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">Video</th>
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">status</th>
                              <th class="text-center" style="border:1px solid #d2d6de;padding:8px;">action</th>
                            </tr>
                            
                            <tr class="text-center refresh">
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                                  <video src="{{ url('imgs/product/video/'.$productvideo->name) }}" height="100"></video>
                                </td>
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;;">
                                    <!-- <input data-id="{{$productvideo->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" {{ $productvideo->status ? 'checked' : '' }}> -->
                                    <select data-type="video" data-shop_id="{{$edit->shop_id}}" data-product_id="{{$edit->id}}" data-id="{{$productvideo->id}}" name="type" class="change_status">
                                      <option value="Approve" {{ $productvideo->type == 'Approve' ? 'selected' : '' }} >Approve</option>
                                      <option value="In-progress" {{ $productvideo->type == 'In-progress' ? 'selected' : '' }}>In-progress</option>
                                      <option value="Reject" {{ $productvideo->type == 'Reject' ? 'selected' : '' }}>Reject</option>
                                    </select>
                                  </td>
                                <td class="text-center" style="border:1px solid #d2d6de;padding:8px;">
                                    <a class="btn-floating remove-image" data-type="video" data-id="{{ $productvideo->id }}"><i class="fas fa-trash-alt text-danger p-1"></i></a>
                                </td>
                                
                            </tr>
                        </table>
                          <!--<div class="row">-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--    <div class="col-lg-4">-->
                              
                          <!--    </div>-->
                          <!--</div>-->
                        
                        </div>
                        @else 
                        @endif
                        </div>
                      </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                          {{Form::label('image', 'Select Product Image')}}
                          {{Form::file('image', ['class' => 'form-control', 'placeholder'=>'Enter Product Image','id'=>'image'])}}
                        </div>
                        <img src="{{ url('imgs/product/'.$edit->image) }}" alt="" width="50"><br>

                        <div class="form-group">
                          {{Form::label('video', 'Select Product Video')}}
                          {{Form::file('video', ['class' => 'form-control', 'id'=>'title'])}}
                        </div>
                        <br>
                      
                      {{Form::label('images', 'Multiple Image')}}
                      <div class="input-group control-group increment mb-3" >
                        <input type="file" name="images[]" class="form-control">
                        <div class="input-group-btn"> 
                          <button class="btn btn-success input-add" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                        </div>
                      </div>
                      <div class="clone hide mb-3">
                        <div class="control-group input-group" style="margin-top:10px; margin-bottom:10px">
                          <input type="file" name="images[]" class="form-control">
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger input-remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      </div>
                  <div class="text-right">
                    <input type="submit"class="btn btn-primary" value="Edit Product"/>
                  </div>
                {!! Form::close() !!}
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Reject Reason</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body" >
                                      {!! Form::open(['route'=> array('changeproductStatus'), 'class' => 'reject_change_status mb-5']) !!}
                                        <textarea name="message" id="message" rows="4" placeholder="reason..." style="width:100%"></textarea><br>
                                        <input type="hidden" name="" id="data_type" value="">
                                        <input type="hidden" name="" id="id" value="">
                                        <input type="hidden" name="" id="shop_id" value="">
                                        <input type="hidden" name="" id="product_id" value="">
                                        <div style="float:right">
                                          <input type="submit" value="Submit" class="btn btn-success">
                                          <input type="button" value="Cancel" class="btn btn-danger" data-dismiss="modal">
                                        </div>
                                      {!! Form::close() !!}
                                    </div>
                                    <!-- <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                  </div>
                                  
                                </div>
                              </div>
                </div>
              </div>
            </div>
          </div>
         </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
