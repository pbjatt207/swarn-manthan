<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
       @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Content Row -->
          @if (\Session::has('success'))
              <div class="alert alert-success toast-msg" style="color: green">
                  {!! \Session::get('success') !!}</li>
              </div>
          @endif

          @if (\Session::has('danger'))
              <div class="alert alert-danger toast-msg" style="color: red;">
                  {!! \Session::get('danger') !!}</li>
              </div>
          @endif
          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Product</h6>
                  <a href="{{ url('admin/product') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> View Products</a>
                </div>

                 <!-- Card Body -->
                <div class="card-body" id="category_box">
                {!! Form::open(['method' => 'POST', 'action' => 'admin\ProductController@store', 'class' => 'user', 'files'=>'true']) !!}
                      @include('admin.inc.product._form')
                      <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          {{Form::label('image', 'Select Product Image')}}
                          {{Form::file('image', ['class' => 'form-control', 'placeholder'=>'Enter Product Image','id'=>'title','required'=>'required'])}}
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          {{Form::label('video', 'Select Product Video')}}
                          {{Form::file('video', ['class' => 'form-control', 'id'=>'title'])}}
                        </div>
                      </div>
                      <div class="col-lg-6">
                      {{Form::label('images', 'Multiple Image( limit 3 Images )')}}
                      <div class="input-group control-group increment mb-3" >
                        <input type="file" name="images[]" class="form-control">
                        <div class="input-group-btn"> 
                          <button class="btn btn-success input-add" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                        </div>
                      </div>
                      <div class="clone hide mb-3">
                        <div class="control-group input-group" style="margin-top:10px; margin-bottom:10px">
                          <input type="file" name="images[]" class="form-control">
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger input-remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      </div>
                  <div class="text-right">
                    <input type="submit"class="btn btn-primary" value="Add Product"/>
                  </div>
                {!! Form::close() !!}
                </div>
              </div>
            </div>

          </div>
         </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
