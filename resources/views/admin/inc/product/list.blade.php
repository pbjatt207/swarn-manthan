<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-9"></div>
            <div class="col-lg-3">

              {{ Form::open(['url' => url(env('ADMIN_DIR').'/product'),'method'=>'GET', 'class'=>'mb-3']) }}
              
                <div class="input-group " style="">
      
                  {!! Form::text('search','',['class' => 'form-control', 'placeholder' => 'Search...'])!!}
                  <div class="input-group-append">
                    
                    {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                    <!-- <button class="btn btn-primary" data-url="{{ url(env('ADMIN_DIR').'/product') }}" >Search</button> -->
                  </div>
                </div>
              {{ Form::close() }}
            </div>
          </div>
          {{ Form::open(['url' => url(env('ADMIN_DIR').'/product')]) }}
          <!-- Page Heading -->
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="card">
                <div class="card-header d-sm-flex align-items-center justify-content-between mb-4">
                  <h6 class="m-0 font-weight-bold text-primary">Product List</h6>
                  <div class="">
                    <a href="{{ route('product.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">+ Add Product</a>
                    <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-url="{{ url(env('ADMIN_DIR').'/product/delete') }}" id="delete_all">Delete</button>

                  </div>
                </div>
                <div class="row card-body">
                  
                  <div class="col-lg-9 float-left" style="font-size: 18px;">{{ $lists->firstItem() }} - {{ $lists->lastItem() }} out of {{ $lists->total() }} record(s) showing.  </div>
                  <div class="col-lg-3 float-left" style="font-size: 18px;">
                    
                  </div>
                </div>
                <div class="card-body">
                @if( !$lists->isEmpty() )
                  <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead class="thead-dark">
                      <tr>
                        <th>S No.</th>
                        <th>Image</th>
                        <th>Images</th>
                        <th>Name</th>
                        <th>Shop</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($lists as $key => $list)
                      <tr class="bg-light">
                        <td>{{ $key+1 }}. |
                          <input type="checkbox" name="sub_chk[]" value="{{ $list->id }}" class="sub_chk" data-id="{{$list->id}}">
                        </td>
                        <td>
                        <img src="{{ url('public/imgs/product/'.$list->image) }}" style="width:50px"/>
                        </td>
                        <td>
                          @foreach($list->images as $ml)
                          <img src="{{ url('public/imgs/product/'.$ml->name) }}" style="width:50px"/>
                          @endforeach
                          <!-- @if( $list->video  != '' )
                          Video <br>
                          <video src="{{ url('imgs/product/video/'.$list->video->name) }}" height="50px"></video>
                          @endif -->
                        </td>
                        <td>
                          <a href="{{route('product.edit', $list->id) }}">
                            <i class="far fa-edit" aria-hidden="true"></i> 
                            {{$list->name}}
                          </a>
                        </td>
                        <td>{{$list->shop->name}}</td>
                        <td>{{$list->category->name}}</td>
                        <td>{{$list->price}}</td>
                        <!-- <td>
                           <a href="{{url(env('ADMIN_DIR').'/product/delete', $list->id)}}"  class="btn btn-danger btn-sm"
                            data-tr="tr_{{$list->id}}"
                            data-toggle="confirmation"
                            data-btn-ok-label="Delete" data-btn-ok-icon="fa fa-remove"
                            data-btn-ok-class="btn btn-sm btn-danger"
                            data-btn-cancel-label="Cancel"
                            data-btn-cancel-icon="fa fa-chevron-circle-left"
                            data-btn-cancel-class="btn btn-sm btn-default"
                            data-title="Are you sure you want to delete ?"
                            data-placement="left" data-singleton="true"
                            onclick="return confirm('Are you sure you want to delete this item?');">
                             Delete</a>
                         </td> -->
                        <td>
                          @if($list->status  == "true")
                          <a href="{{ route('productchangestatus', [$list->id, 'field' => 'status', 'status' => 'false', 'id' => $list->id]) }}" class="btn btn-success text-white">Enable</a>
                          @else
                          <a href="{{ route('productchangestatus', [$list->id, 'field' => 'status', 'status' => 'true', 'id' => $list->id]) }}" class="btn btn-warning text-white">Disable</a>
                          @endif
                        </td>
                      </tr>

                      @endforeach
                    </tbody>
                  </table>
                  {{ $lists->links() }}
                  @else
                    No Record Found
                    @endif
                </div>

              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>