@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="row">

  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('title', 'Enter Title')}}
      {{Form::text('title', '', ['class' => 'form-control', 'placeholder'=>'Enter title','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('slug', 'Enter Slug')}}
      {{Form::text('slug', '', ['class' => 'form-control', 'placeholder'=>'Enter slug','id'=>'title'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('role_id', 'Select Role')}}
      {{Form::select('role_id', $roleArr,'', ['class' => 'form-control','id'=>'role_id'])}}
    </div>
  </div>

  <div class="col-lg-12">
    <div class="form-group">
      {{Form::label('description', 'Enter description')}}
      {{Form::textarea('description', '', ['class' => 'form-control editor','id'=>'description', 'placeholder'=>'Enter short description','rows'=>'4', 'col'=>'3'])}}
    </div>
  </div>
</div>