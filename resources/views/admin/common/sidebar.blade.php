<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('admin/home') }}">
    <!-- <img src="{{ url('imgs/logo/logo.png') }}" alt="" style="max-height: 50px;"> -->
    Swarn Manthan
  </a>
  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="{{  url('admin/home') }}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>

  <hr class="sidebar-divider">

  <div class="sidebar-heading">
    Master
  </div>





  <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#timeslot" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-folder"></i>
          <span>Marchant</span>
        </a>
        <div id="timeslot" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            
            <a class="collapse-item" href="{{  url('admin/timeslot/?g_type=yantra') }}">Add</a>
            <a class="collapse-item" href="{{  url('admin/timeslot/?g_type=city') }}">City</a>
          </div>
        </div>
      </li> -->

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#yantra" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-fw fa-folder"></i>
      <span>Master</span>
    </a>
    <div id="yantra" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">

        <a class="collapse-item" href="{{ route('state.index') }}">State</a>
        <a class="collapse-item" href="{{ route('city.index') }}">City</a>
        <a class="collapse-item" href="{{ route('plan.index') }}">Plan</a>
        <a class="collapse-item" href="{{ route('role.index') }}">Role</a>
      </div>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/staff') }}">
      <i class="fas fa-fw fa-user"></i>
      <span>Staff</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/marchant') }}">
      <i class="fas fa-fw fa-user"></i>
      <span>Marchants</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/user_list') }}">
      <i class="fas fa-fw fa-user"></i>
      <span>Users</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/category') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Category</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/product') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Products</span></a>
  </li>
  
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#offer" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-fw fa-user"></i>
      <span>Offer</span>
    </a>
    <div id="offer" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <!-- <a class="collapse-item" href="{{ url('admin/add-category') }}">Category</a> -->
        <a class="collapse-item" href="{{ url('admin/offer/create') }}">Add</a>
        <a class="collapse-item" href="{{ url('admin/offer/') }}">View</a>
      </div>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/plan') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Plans</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/purchase_plan') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Purchase Plans</span></a>
  </li>




  <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#sliderMenu1" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-folder"></i>
          <span>Game yantra</span>
        </a>
        <div id="sliderMenu1" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/game/create?type=yantra') }}">Add</a>
            <a class="collapse-item" href="{{ url('admin/game') }}">View</a>
          </div>
        </div>
      </li> -->


  <!-- Divider -->
  <hr class="sidebar-divider">
  <!-- Heading -->
  <div class="sidebar-heading">
    User
  </div>
  <!-- Nav Item - Utilities Collapse Menu -->

  @php
  $date = date('Y-m-d');
  @endphp
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#enquiry" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-fw fa-user"></i>
      <span>Enquiry</span>
    </a>
    <div id="enquiry" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <!-- <a class="collapse-item" href="{{ url('admin/add-category') }}">Category</a> -->
        <a class="collapse-item" href="{{ url('admin/enquiry/marchant') }}">Marchant</a>
        <a class="collapse-item" href="{{ url('admin/enquiry/user') }}">User</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pages" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-fw fa-user"></i>
      <span>Pages</span>
    </a>
    <div id="pages" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <!-- <a class="collapse-item" href="{{ url('admin/add-category') }}">Category</a> -->
        <a class="collapse-item" href="{{ url('admin/page/create') }}">Add</a>
        <a class="collapse-item" href="{{ url('admin/page/') }}">View</a>
      </div>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('contact-enquery') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Contact Enquery</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('business-enquery') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Business Enquery</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('admin/setting/1/edit') }}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Setting</span></a>
  </li>



  <li class="nav-item">
    <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
      <i class="fas fa-sign-out-alt fa-chart-area"></i>
      <span>LogOut</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->