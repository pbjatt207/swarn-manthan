@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-2">
    <div class="form-group">
      {{Form::label('name', 'Enter plan name')}}
      {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter plan name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      {{Form::label('price', 'Enter Plan Price')}}
      {{Form::number('record[price]', '', ['class' => 'form-control', 'placeholder'=>'Enter plan price','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      {{Form::label('product_limit', 'Enter Product Limit')}}
      {{Form::number('record[product_limit]', '', ['class' => 'form-control', 'placeholder'=>'Enter product limit','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      {{Form::label('time_limit', 'Plan Duration')}}
      {{Form::number('record[time_limit]', '', ['class' => 'form-control', 'placeholder'=>'Enter plan duration','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      @php 
      $time_limit_in = [
          'Day'   => 'Day',
          'Month' => 'Month',
          'Year'  => 'Year',
        ];
      @endphp
      {{Form::label('time_limit_in', 'Plan Duration In')}}
      {{Form::select('record[time_limit_in]', $time_limit_in,'0', ['class' => 'form-control', 'id'=>'time_limit_in','required'=>'required'])}}
      
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      @php 
      $typeArr = [
          'user'   => 'User',
          'marchant' => 'Marchant',
        ];
      @endphp
      {{Form::label('type', 'Plan Type')}}
      {{Form::select('record[type]', $typeArr,'0', ['class' => 'form-control', 'id'=>'type','required'=>'required'])}}
      
    </div>
  </div>

</div>