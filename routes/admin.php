<?php

use App\Http\Controllers\admin\WinController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest', 'namespace' => 'admin'], function () {
    Route::any('', 'UserController@index')->name('admin_login');
    Route::post('main/checklogin', 'UserController@checklogin');
});



Route::group(['middleware' => 'auth:admin', 'namespace' => 'admin'], function () {

    // Dashboard
    Route::get('home', 'DashboardController@index')->name('admin_home');

    // User 
    // Route::resource('user', 'UsersController');
    Route::post('user/delete', 'UsersController@destroyAll');
    Route::post('state/delete', 'StateController@destroyAll');
    Route::post('city/delete', 'CityController@destroyAll');
    Route::post('plan/delete', 'PlanController@destroyAll');
    Route::post('role/delete', 'RoleController@destroyAll');
    Route::post('marchant/delete', 'UsersController@destroyAll');
    Route::get('user_list', 'UsersController@userlist');
    Route::get('enquiry/marchant', 'EnqueryController@marchantEnquiry');
    Route::get('enquiry/user', 'EnqueryController@userEnquiry');
    Route::post('offer/delete', 'OfferController@destroyAll');
    Route::post('product/delete', 'ProductController@destroyAll');
    Route::post('category/delete', 'CategoryController@destroyAll');
    Route::get('product/changeStatus', 'ProductController@changeproductStatus')->name('changeproductStatus');
    Route::get('product/remove-image', 'ProductController@removeProductimage')->name('removeProductimage');

    // Master 
    Route::resources([
        'page'      => 'PagesController',
        'state'     => 'StateController',
        'city'      => 'CityController',
        'role'      => 'RoleController',
        'offer'     => 'OfferController',
        'marchant'  => 'UsersController',
        'staff'     => 'StaffController',
        'product'   => 'ProductController',
        'category'  => 'CategoryController',
        'plan'      => 'PlanController'
    ]);

    Route::get('purchase_plan', 'PlanController@purchase_plan')->name('purchase_plan');
    Route::get('purchase_plan/status/{user}', 'PlanController@purchaseplanchangestatus')->name('purchagePlanchangestatus');
    Route::get('marchant/status/{user}', 'UsersController@changestatus')->name('marchantchangestatus');
    Route::get('product/status/{user}', 'ProductController@changestatus')->name('productchangestatus');

    Route::get('contact-enquery', 'ContactController@index')->name('contact-enquery');
    Route::get('contact-enquiry/status/{id}', 'ContactController@changestatus')->name('contact-enquiry-status');
    Route::post('contact-enquiry/delete', 'ContactController@destroyAll')->name('contact-deleteall');

    Route::get('business-enquery', 'ContactController@business_index')->name('business-enquery');
    Route::get('business-enquiry/status/{id}', 'ContactController@business_changestatus')->name('business-enquiry-status');
    Route::post('business-enquiry/delete', 'ContactController@business_destroyAll')->name('business-deleteall');

    // page 
    Route::post('page/delete', 'PagesController@destroyAll');

    // setting 
    Route::resource('setting', 'SettingController');


    Route::get('logout', 'UserController@logout')->name('admin_logout');
    Route::get('general-setting', 'SettingController@edit')->name('general_setting');
});
