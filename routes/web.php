<?php

use Illuminate\Support\Facades\Route;


Route::get('/migrate', function () {
    \Artisan::call('migrate');
    return 'migrate';
});
Route::any('admin', function () {
    return false;
});
// Route::get('/passport', function () {
//     $exitCode = Artisan::call('passport:install');
//     return 'clearede';
// });


Route::get('', 'HomeController@index')->name('home');
Route::get('/contact-us', 'HomeController@contact')->name('contact');
Route::get('/business-enquery', 'HomeController@business_enquery')->name('business');
Route::post('/business-enquery', 'HomeController@business_enquery_post')->name('businesspost');
Route::get('/our-partners', 'HomeController@ourpartner')->name('ourpartner');
Route::get('/{slug}', 'HomeController@page')->name('page');
Route::get('page/{role}/{slug}', 'HomeController@otherpage')->name('other-pages');
Route::post('/contact', 'HomeController@contact_enquery')->name('contact-us');
Route::get('/shop/{slug}', 'HomeController@products')->name('products');
Route::get('marchant/create/{id}', 'MarchantController@index');
Route::post('marchant/create/{id}', 'MarchantController@add_marchant')->name('add_marchant');


// Route::group(['middleware' => 'guest', 'namespace' => 'admin'], function () {
//     Route::any('/', 'UserController@index')->name('admin_login');
//     Route::post('main/checklogin', 'UserController@checklogin');
// });
