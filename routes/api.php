<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Marchant Api
Route::group(['namespace' => 'api'], function () {
    Route::post('/send-otp', 'LoginController@sendOtp');
    Route::post('/login', 'LoginController@login');
    Route::post('/otp_verify', 'LoginController@verifyOtp');
    Route::post('/forgotpassword', 'LoginController@forgotpassword');
    Route::get('/page/{slug}', 'PagesController@page');
    Route::get('/pages', 'PagesController@pages');
    Route::get('/user/page/{slug}', 'PagesController@userpage');
    Route::get('/user/pages', 'PagesController@userpages');

    Route::get('/city', 'HomeController@city');

    Route::get('/product_detail', 'ProductController@product_detail');

    Route::get('/search_product', 'ProductController@Search_product');
});

// User Api 
Route::group(['namespace' => 'api'], function () {
    Route::post('/users/register', 'RegisterController@register');
    Route::post('/users/login', 'UserloginController@login');
    Route::post('/users/send-otp', 'RegisterController@sendOtp');
    Route::post('/verifyotp', 'RegisterController@verifyOtp');
    Route::get('/category', 'CategoryController@index');
    Route::get('/mail', 'MailController@send');
    Route::get('/category/{id}', 'CategoryController@product');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/users/editprofile', 'UserloginController@edit_profile');


        Route::get('/users/enquery_list', 'EnqueryController@userenquery');

        Route::get('/users/home', 'HomeController@index');
        Route::post('/add_wishlist', 'WishlistController@add_wishlist');
        Route::post('/remove_wishlist', 'WishlistController@remove_wishlist');
        Route::get('/show_wishlist', 'WishlistController@show_wishlist');
        Route::get('/product_list', 'ProductController@product_list');
        Route::get('users/product_detail', 'ProductController@user_product_detail');
        //     Route::get('/shop', 'ShopController@index');  
        //     Route::post('/shop/edit', 'ShopController@update');  

        //     Route::get('/marchant/enquery', 'MarchantenqueryController@index');  
        //     Route::get('/marchant/enquery/{enquery}', 'MarchantenqueryController@show'); 

        Route::group(['prefix' => 'users'], function () {
            Route::resources([
                'enquery'         => 'EnqueryController',
            ]);
        });
    });
});

Route::group(['namespace' => 'api'], function () {
    Route::post('marchant_list', 'ShopController@marchant_list');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/editprofile', 'LoginController@edit_profile');
        Route::post('/changepassword', 'LoginController@change_password');

        Route::get('/logout', 'LoginController@logout');

        Route::get('/shop', 'ShopController@index');
        Route::post('/shop/edit', 'ShopController@update');

        Route::get('/marchant/enquery', 'MarchantenqueryController@index');
        Route::get('/marchant/enquery/{enquery}', 'MarchantenqueryController@show');

        Route::get('/marchant_notification', 'NotificationController@Marchant_notification');
        Route::get('/user_notification', 'NotificationController@User_notification');
        Route::get('/notification/{notification}', 'NotificationController@show');

        Route::post('/add_product_image', 'ProductController@add_product_image');
        Route::post('/latitude_longitude', 'ShopController@latitude_longitude');
        Route::get('/remove_product_image', 'ProductController@remove_product_image');
        Route::get('/plan_list', 'PlanController@plan_list');
        Route::post('/purchase_plan', 'PlanController@PurchasePlan');

        Route::resources([
            'product'         => 'ProductController',
            'enquery'         => 'EnqueryController',
        ]);
    });
});
